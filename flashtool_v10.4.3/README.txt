"""
Copyright (C) 2019, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.

Module:         GEN3_FLASH_UTILITY
Date Created:   30th Nov 2021

Description: This module allows flashing/erasing compiled C code and calibration data partition via CAN interface to ISG GEN3 family ECUs from SEDEMAC
"""

Instructions:

To flash code -
1. Ensure a main.mot file is present in the directory. Replace file with new file if needed.
2. Double click on the code_flash.bat file

To force flash code - 
1. Ensure a main.mot file is present in the directory. Replace file with new file if needed.
2. Power off ECU and wait till capacitors are discharged.
3. Double click on code_flash_force.bat file.
4. Power on the ECU.

To erase code -
1. Double click on the code_erase.bat file

To erase calibration data in flash
1. Double click on the calib_data_erase.bat file

To create a single mot file by merging mots of bootloader and application -
1. Edit the merge_files.bat to give the address of the bootloader and application's mot files.
2. Save and close
3. Double click on the merge_files.bat.
4. New file name.ecuimg file will be created.
5. Remove the ".ecuimg" part from the name.
