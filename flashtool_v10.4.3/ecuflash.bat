@echo off
set opratingSec=%1
set oprTyp=%2
set hexFilePath=%3

: Step I: Argument validation and information extraction.
if [%opratingSec%]==[] (
    echo Section not specified.
    goto :helpInfo
)

if [%oprTyp%]==[] (
    echo Operation to be performed is not specified.
    goto :helpInfo
)

: Step II: Initialize variables.
set nCodePrtn=0
set cpStart=0
set cpSize=0
set nDataPrtn=0
set dpStart=0
set dpSize=0

: Step IV: Execute specified operation
:ExecProg
    if %oprTyp%==-erase (
        : Arguments to perform only erase operation
        ecuflash.exe -erase -mcu MCU_SERIES_RENESAS_RH850 -prot SED -baudrate 1000 1000 -crc CRC_CCITT -funcid 0x33 -ncp 1 -fswmem 0x00010080 0x6FF80
        goto :end
    ) else if %oprTyp%==-flash (
        if [%hexFilePath%]==[] (
            echo Hex file path+name not specified.
            goto :helpInfo
        ) else (
            : Arguments to update image in ECU
            ecuflash.exe -flash "%hexFilePath%" -mcu MCU_SERIES_RENESAS_RH850 -prot SED -baudrate 1000 1000 -crc CRC_CCITT -funcid 0x33 -ncp 1 -fswmem 0x00010080 0x6FF80
            goto :end
        )
    ) else (
        echo Invalid operation option specified
        set errorlevel=-1
        goto :helpInfo
    )


:helpInfo
    echo.
    echo SEDEMAC ECU flashing script.
    echo Kindly provide following 2 arguments in given sequence.
    echo     e.g. {file path + name}.bat {arg1} {arg2}
    echo.
    echo     {arg1} : a section type, on which operation specified by {arg2} will be performed.
    echo              Use any of the option from following list to erase or (re)program
    echo              respective partition.
    echo                  -code
    echo.
    echo     {arg2} : operation to be performed on specified section
    echo              -erase:
    echo                  specify this option to erase stated partition(s).
    echo              -flash [filename]:
    echo                  use this option to download(erase+flash) partition(s).
    echo                  specify the name of the s19/s28/s37 file to download.

    goto :end

:end
    exit /b %errorlevel%