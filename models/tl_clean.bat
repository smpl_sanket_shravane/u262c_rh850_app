@echo off

if "%~1" == "" (
  SET _Mode=all
) ELSE (
  SET _Mode=%~1
)

IF "%_Mode%" == "CGU_NAME_LIST" (
echo *cosine*
echo *sine*
echo *pi_controller*
echo *arctan*
echo *park_transform*
echo *bemf_obsrv_dq_update*
echo *bemf_tracker_update*
echo *clarke_transform*
echo *square_root*
echo *inverse_park*
echo *svm*
echo *elim_dc_bus_ripple*
echo *bemf_pid_gains_update*
echo *bemf_obsrv_gains_update*
echo *tracker_pid_gains_update*
echo *custom_observer_bemf_6Step*
echo *observer_bhspin_lib*
echo *MIL*
echo *PID*
echo *Gain_Scheduling*
echo *Controller*
GOTO :EOF
)

IF "%_Mode%" == "all" (

rem *********************************************************************************************
rem ** Cleanup all TargetLink predefined directories. 
rem *********************************************************************************************
if exist ".\TLProj" rmdir /s /q ".\TLProj"
if exist ".\CodeViewFiles" rmdir /s /q ".\CodeViewFiles"
if exist ".\CodeCoverageReport" rmdir /s /q ".\CodeCoverageReport"
if exist ".\doc" rmdir /s /q ".\doc"
if exist ".\TLSim" rmdir /s /q ".\TLSim"
if exist ".\TLBuild" rmdir /s /q ".\TLBuild"

rem *********************************************************************************************
rem ** Cleanup code generation unit specific directories. 
rem *********************************************************************************************
call:delete_cosine_files
call:delete_sine_files
call:delete_pi_controller_files
call:delete_arctan_files
call:delete_park_transform_files
call:delete_bemf_obsrv_dq_update_files
call:delete_bemf_tracker_update_files
call:delete_clarke_transform_files
call:delete_square_root_files
call:delete_inverse_park_files
call:delete_svm_files
call:delete_elim_dc_bus_ripple_files
call:delete_bemf_pid_gains_update_files
call:delete_bemf_obsrv_gains_update_files
call:delete_tracker_pid_gains_update_files
call:delete_custom_observer_bemf_6step_files
call:delete_observer_bhspin_lib_files
call:delete_mil_files
call:delete_pid_files
call:delete_gain_scheduling_files
call:delete_controller_files

rem *********************************************************************************************
rem ** cleanup ASAP2 files and files created during A2L file generation
rem *********************************************************************************************
if exist a2lexport.log                del a2lexport.log
if exist a2l_export_param.dd          del a2l_export_param.dd
if exist asap2_all_in_one.xsl         del asap2_all_in_one.xsl
if exist asap2_all_in_one_module.xsl  del asap2_all_in_one_module.xsl
if exist asap2_default.xsl            del asap2_default.xsl
if exist SymTabParser.cfg             del SymTabParser.cfg
if exist *.a2l                        del *.a2l

rem *********************************************************************************************
rem ** cleanup other files
rem *********************************************************************************************
if exist tl_autoscaling.log           del tl_autoscaling.log
if exist dsdd_validate.log            del dsdd_validate.log
if exist lnk*.tmp                     del lnk*.tmp
if exist make*.lck                    del make*.lck
if exist vc40.pdb                     del vc40.pdb
if exist vc50.pdb                     del vc50.pdb
if exist vc*.idb                      del vc*.idb
if exist NONE                         del NONE
if exist BS_Debug.txt                 del BS_Debug.txt
if exist hdi.hdc                      del hdi.hdc
if exist hdi.hds                      del hdi.hds
if exist tl_diff_cgopt_report.log     del tl_diff_cgopt_report.log

GOTO :EOF
)

IF "%_Mode%" == "cosine" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit cosine. 
rem *********************************************************************************************
call:delete_cosine_files
GOTO :EOF
)

IF "%_Mode%" == "sine" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit sine. 
rem *********************************************************************************************
call:delete_sine_files
GOTO :EOF
)

IF "%_Mode%" == "pi_controller" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit pi_controller. 
rem *********************************************************************************************
call:delete_pi_controller_files
GOTO :EOF
)

IF "%_Mode%" == "arctan" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit arctan. 
rem *********************************************************************************************
call:delete_arctan_files
GOTO :EOF
)

IF "%_Mode%" == "park_transform" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit park_transform. 
rem *********************************************************************************************
call:delete_park_transform_files
GOTO :EOF
)

IF "%_Mode%" == "bemf_obsrv_dq_update" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit bemf_obsrv_dq_update. 
rem *********************************************************************************************
call:delete_bemf_obsrv_dq_update_files
GOTO :EOF
)

IF "%_Mode%" == "bemf_tracker_update" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit bemf_tracker_update. 
rem *********************************************************************************************
call:delete_bemf_tracker_update_files
GOTO :EOF
)

IF "%_Mode%" == "clarke_transform" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit clarke_transform. 
rem *********************************************************************************************
call:delete_clarke_transform_files
GOTO :EOF
)

IF "%_Mode%" == "square_root" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit square_root. 
rem *********************************************************************************************
call:delete_square_root_files
GOTO :EOF
)

IF "%_Mode%" == "inverse_park" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit inverse_park. 
rem *********************************************************************************************
call:delete_inverse_park_files
GOTO :EOF
)

IF "%_Mode%" == "svm" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit svm. 
rem *********************************************************************************************
call:delete_svm_files
GOTO :EOF
)

IF "%_Mode%" == "elim_dc_bus_ripple" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit elim_dc_bus_ripple. 
rem *********************************************************************************************
call:delete_elim_dc_bus_ripple_files
GOTO :EOF
)

IF "%_Mode%" == "bemf_pid_gains_update" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit bemf_pid_gains_update. 
rem *********************************************************************************************
call:delete_bemf_pid_gains_update_files
GOTO :EOF
)

IF "%_Mode%" == "bemf_obsrv_gains_update" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit bemf_obsrv_gains_update. 
rem *********************************************************************************************
call:delete_bemf_obsrv_gains_update_files
GOTO :EOF
)

IF "%_Mode%" == "tracker_pid_gains_update" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit tracker_pid_gains_update. 
rem *********************************************************************************************
call:delete_tracker_pid_gains_update_files
GOTO :EOF
)

IF "%_Mode%" == "custom_observer_bemf_6step" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit custom_observer_bemf_6step. 
rem *********************************************************************************************
call:delete_custom_observer_bemf_6step_files
GOTO :EOF
)

IF "%_Mode%" == "observer_bhspin_lib" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit observer_bhspin_lib. 
rem *********************************************************************************************
call:delete_observer_bhspin_lib_files
GOTO :EOF
)

IF "%_Mode%" == "mil" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit mil. 
rem *********************************************************************************************
call:delete_mil_files
GOTO :EOF
)

IF "%_Mode%" == "pid" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit pid. 
rem *********************************************************************************************
call:delete_pid_files
GOTO :EOF
)

IF "%_Mode%" == "gain_scheduling" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit gain_scheduling. 
rem *********************************************************************************************
call:delete_gain_scheduling_files
GOTO :EOF
)

IF "%_Mode%" == "controller" (

rem *********************************************************************************************
rem ** Cleanup files generated for code generation unit controller. 
rem *********************************************************************************************
call:delete_controller_files
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit cosine
rem *********************************************************************************************


:delete_cosine_files
set tl_subsys=cosine
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\cosine_fri.c" del ".\TLSim\cosine_fri.c" 
if exist ".\TLSim\cosine_fri.h" del ".\TLSim\cosine_fri.h" 
if exist ".\TLSim\cosine_sf.c" del ".\TLSim\cosine_sf.c" 
if exist ".\TLSim\cosine_pcf.c" del ".\TLSim\cosine_pcf.c" 
if exist ".\TLSim\cosine_frm.h" del ".\TLSim\cosine_frm.h" 
if exist ".\TLSim\globals_cosine.*" del ".\TLSim\globals_cosine.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\cosine\Metadata\cosine_SubsystemObject.dd" del ".\TLProj\cosine\Metadata\cosine_SubsystemObject.dd" 
if exist ".\TLProj\cosine" rmdir /s /q ".\TLProj\cosine" 
if exist ".\TLSim\StubCode\cosine" rmdir /s /q ".\TLSim\StubCode\cosine" 
if exist ".\TLProj\cosine\CGReport" rmdir /s /q ".\TLProj\cosine\CGReport" 
if exist ".\TLProj\cosine\Metadata" call:delete_if_empty ".\TLProj\cosine\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\cosine" call:delete_if_empty ".\TLProj\cosine"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit sine
rem *********************************************************************************************


:delete_sine_files
set tl_subsys=sine
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\sine_fri.c" del ".\TLSim\sine_fri.c" 
if exist ".\TLSim\sine_fri.h" del ".\TLSim\sine_fri.h" 
if exist ".\TLSim\sine_sf.c" del ".\TLSim\sine_sf.c" 
if exist ".\TLSim\sine_pcf.c" del ".\TLSim\sine_pcf.c" 
if exist ".\TLSim\sine_frm.h" del ".\TLSim\sine_frm.h" 
if exist ".\TLSim\globals_sine.*" del ".\TLSim\globals_sine.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\sine\Metadata\sine_SubsystemObject.dd" del ".\TLProj\sine\Metadata\sine_SubsystemObject.dd" 
if exist ".\TLProj\sine" rmdir /s /q ".\TLProj\sine" 
if exist ".\TLSim\StubCode\sine" rmdir /s /q ".\TLSim\StubCode\sine" 
if exist ".\TLProj\sine\CGReport" rmdir /s /q ".\TLProj\sine\CGReport" 
if exist ".\TLProj\sine\Metadata" call:delete_if_empty ".\TLProj\sine\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\sine" call:delete_if_empty ".\TLProj\sine"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit pi_controller
rem *********************************************************************************************


:delete_pi_controller_files
set tl_subsys=pi_controller
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\pi_controller_fri.c" del ".\TLSim\pi_controller_fri.c" 
if exist ".\TLSim\pi_controller_fri.h" del ".\TLSim\pi_controller_fri.h" 
if exist ".\TLSim\pi_controller_sf.c" del ".\TLSim\pi_controller_sf.c" 
if exist ".\TLSim\pi_controller_pcf.c" del ".\TLSim\pi_controller_pcf.c" 
if exist ".\TLSim\pi_controller_frm.h" del ".\TLSim\pi_controller_frm.h" 
if exist ".\TLSim\globals_pi_controller.*" del ".\TLSim\globals_pi_controller.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\pi_controller\Metadata\pi_controller_SubsystemObject.dd" del ".\TLProj\pi_controller\Metadata\pi_controller_SubsystemObject.dd" 
if exist ".\TLProj\pi_controller" rmdir /s /q ".\TLProj\pi_controller" 
if exist ".\TLSim\StubCode\pi_controller" rmdir /s /q ".\TLSim\StubCode\pi_controller" 
if exist ".\TLProj\pi_controller\CGReport" rmdir /s /q ".\TLProj\pi_controller\CGReport" 
if exist ".\TLProj\pi_controller\Metadata" call:delete_if_empty ".\TLProj\pi_controller\Metadata"
if exist ".\TLProj\pi_controller" call:delete_if_empty ".\TLProj\pi_controller"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit arctan
rem *********************************************************************************************


:delete_arctan_files
set tl_subsys=arctan
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\arctan_fri.c" del ".\TLSim\arctan_fri.c" 
if exist ".\TLSim\arctan_fri.h" del ".\TLSim\arctan_fri.h" 
if exist ".\TLSim\arctan_sf.c" del ".\TLSim\arctan_sf.c" 
if exist ".\TLSim\arctan_pcf.c" del ".\TLSim\arctan_pcf.c" 
if exist ".\TLSim\arctan_frm.h" del ".\TLSim\arctan_frm.h" 
if exist ".\TLSim\globals_arctan.*" del ".\TLSim\globals_arctan.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\arctan\Metadata\arctan_SubsystemObject.dd" del ".\TLProj\arctan\Metadata\arctan_SubsystemObject.dd" 
if exist ".\TLProj\arctan" rmdir /s /q ".\TLProj\arctan" 
if exist ".\TLSim\StubCode\arctan" rmdir /s /q ".\TLSim\StubCode\arctan" 
if exist ".\TLProj\arctan\CGReport" rmdir /s /q ".\TLProj\arctan\CGReport" 
if exist ".\TLProj\arctan\Metadata" call:delete_if_empty ".\TLProj\arctan\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\arctan" call:delete_if_empty ".\TLProj\arctan"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit park_transform
rem *********************************************************************************************


:delete_park_transform_files
set tl_subsys=park_transform
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\park_transform_fri.c" del ".\TLSim\park_transform_fri.c" 
if exist ".\TLSim\park_transform_fri.h" del ".\TLSim\park_transform_fri.h" 
if exist ".\TLSim\park_transform_sf.c" del ".\TLSim\park_transform_sf.c" 
if exist ".\TLSim\park_transform_pcf.c" del ".\TLSim\park_transform_pcf.c" 
if exist ".\TLSim\park_transform_frm.h" del ".\TLSim\park_transform_frm.h" 
if exist ".\TLSim\globals_park_transform.*" del ".\TLSim\globals_park_transform.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\park_transform\Metadata\park_transform_SubsystemObject.dd" del ".\TLProj\park_transform\Metadata\park_transform_SubsystemObject.dd" 
if exist ".\TLProj\park_transform" rmdir /s /q ".\TLProj\park_transform" 
if exist ".\TLSim\StubCode\park_transform" rmdir /s /q ".\TLSim\StubCode\park_transform" 
if exist ".\TLProj\park_transform\CGReport" rmdir /s /q ".\TLProj\park_transform\CGReport" 
if exist ".\TLProj\park_transform\Metadata" call:delete_if_empty ".\TLProj\park_transform\Metadata"
if exist ".\TLProj\park_transform" call:delete_if_empty ".\TLProj\park_transform"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit bemf_obsrv_dq_update
rem *********************************************************************************************


:delete_bemf_obsrv_dq_update_files
set tl_subsys=bemf_obsrv_dq_update
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\bemf_obsrv_dq_update_fri.c" del ".\TLSim\bemf_obsrv_dq_update_fri.c" 
if exist ".\TLSim\bemf_obsrv_dq_update_fri.h" del ".\TLSim\bemf_obsrv_dq_update_fri.h" 
if exist ".\TLSim\bemf_obsrv_dq_update_sf.c" del ".\TLSim\bemf_obsrv_dq_update_sf.c" 
if exist ".\TLSim\bemf_obsrv_dq_update_pcf.c" del ".\TLSim\bemf_obsrv_dq_update_pcf.c" 
if exist ".\TLSim\bemf_obsrv_dq_update_frm.h" del ".\TLSim\bemf_obsrv_dq_update_frm.h" 
if exist ".\TLSim\globals_bemf_obsrv_dq_update.*" del ".\TLSim\globals_bemf_obsrv_dq_update.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\bemf_obsrv_dq_update\Metadata\bemf_obsrv_dq_update_SubsystemObject.dd" del ".\TLProj\bemf_obsrv_dq_update\Metadata\bemf_obsrv_dq_update_SubsystemObject.dd" 
if exist ".\TLProj\bemf_obsrv_dq_update" rmdir /s /q ".\TLProj\bemf_obsrv_dq_update" 
if exist ".\TLSim\StubCode\bemf_obsrv_dq_update" rmdir /s /q ".\TLSim\StubCode\bemf_obsrv_dq_update" 
if exist ".\TLProj\bemf_obsrv_dq_update\CGReport" rmdir /s /q ".\TLProj\bemf_obsrv_dq_update\CGReport" 
if exist ".\TLProj\bemf_obsrv_dq_update\Metadata" call:delete_if_empty ".\TLProj\bemf_obsrv_dq_update\Metadata"
if exist ".\TLProj\bemf_obsrv_dq_update" call:delete_if_empty ".\TLProj\bemf_obsrv_dq_update"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit bemf_tracker_update
rem *********************************************************************************************


:delete_bemf_tracker_update_files
set tl_subsys=bemf_tracker_update
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\bemf_tracker_update_fri.c" del ".\TLSim\bemf_tracker_update_fri.c" 
if exist ".\TLSim\bemf_tracker_update_fri.h" del ".\TLSim\bemf_tracker_update_fri.h" 
if exist ".\TLSim\bemf_tracker_update_sf.c" del ".\TLSim\bemf_tracker_update_sf.c" 
if exist ".\TLSim\bemf_tracker_update_pcf.c" del ".\TLSim\bemf_tracker_update_pcf.c" 
if exist ".\TLSim\bemf_tracker_update_frm.h" del ".\TLSim\bemf_tracker_update_frm.h" 
if exist ".\TLSim\globals_bemf_tracker_update.*" del ".\TLSim\globals_bemf_tracker_update.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\bemf_tracker_update\Metadata\bemf_tracker_update_SubsystemObject.dd" del ".\TLProj\bemf_tracker_update\Metadata\bemf_tracker_update_SubsystemObject.dd" 
if exist ".\TLProj\bemf_tracker_update" rmdir /s /q ".\TLProj\bemf_tracker_update" 
if exist ".\TLSim\StubCode\bemf_tracker_update" rmdir /s /q ".\TLSim\StubCode\bemf_tracker_update" 
if exist ".\TLProj\bemf_tracker_update\CGReport" rmdir /s /q ".\TLProj\bemf_tracker_update\CGReport" 
if exist ".\TLProj\bemf_tracker_update\Metadata" call:delete_if_empty ".\TLProj\bemf_tracker_update\Metadata"
if exist ".\TLProj\bemf_tracker_update" call:delete_if_empty ".\TLProj\bemf_tracker_update"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit clarke_transform
rem *********************************************************************************************


:delete_clarke_transform_files
set tl_subsys=clarke_transform
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\clarke_transform_fri.c" del ".\TLSim\clarke_transform_fri.c" 
if exist ".\TLSim\clarke_transform_fri.h" del ".\TLSim\clarke_transform_fri.h" 
if exist ".\TLSim\clarke_transform_sf.c" del ".\TLSim\clarke_transform_sf.c" 
if exist ".\TLSim\clarke_transform_pcf.c" del ".\TLSim\clarke_transform_pcf.c" 
if exist ".\TLSim\clarke_transform_frm.h" del ".\TLSim\clarke_transform_frm.h" 
if exist ".\TLSim\globals_clarke_transform.*" del ".\TLSim\globals_clarke_transform.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\clarke_transform\Metadata\clarke_transform_SubsystemObject.dd" del ".\TLProj\clarke_transform\Metadata\clarke_transform_SubsystemObject.dd" 
if exist ".\TLProj\clarke_transform" rmdir /s /q ".\TLProj\clarke_transform" 
if exist ".\TLSim\StubCode\clarke_transform" rmdir /s /q ".\TLSim\StubCode\clarke_transform" 
if exist ".\TLProj\clarke_transform\CGReport" rmdir /s /q ".\TLProj\clarke_transform\CGReport" 
if exist ".\TLProj\clarke_transform\Metadata" call:delete_if_empty ".\TLProj\clarke_transform\Metadata"
if exist ".\TLProj\clarke_transform" call:delete_if_empty ".\TLProj\clarke_transform"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit square_root
rem *********************************************************************************************


:delete_square_root_files
set tl_subsys=square_root
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\square_root_fri.c" del ".\TLSim\square_root_fri.c" 
if exist ".\TLSim\square_root_fri.h" del ".\TLSim\square_root_fri.h" 
if exist ".\TLSim\square_root_sf.c" del ".\TLSim\square_root_sf.c" 
if exist ".\TLSim\square_root_pcf.c" del ".\TLSim\square_root_pcf.c" 
if exist ".\TLSim\square_root_frm.h" del ".\TLSim\square_root_frm.h" 
if exist ".\TLSim\globals_square_root.*" del ".\TLSim\globals_square_root.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\square_root\Metadata\square_root_SubsystemObject.dd" del ".\TLProj\square_root\Metadata\square_root_SubsystemObject.dd" 
if exist ".\TLProj\square_root" rmdir /s /q ".\TLProj\square_root" 
if exist ".\TLSim\StubCode\square_root" rmdir /s /q ".\TLSim\StubCode\square_root" 
if exist ".\TLProj\square_root\CGReport" rmdir /s /q ".\TLProj\square_root\CGReport" 
if exist ".\TLProj\square_root\Metadata" call:delete_if_empty ".\TLProj\square_root\Metadata"
if exist ".\TLProj\square_root" call:delete_if_empty ".\TLProj\square_root"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit inverse_park
rem *********************************************************************************************


:delete_inverse_park_files
set tl_subsys=inverse_park
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\inverse_park_fri.c" del ".\TLSim\inverse_park_fri.c" 
if exist ".\TLSim\inverse_park_fri.h" del ".\TLSim\inverse_park_fri.h" 
if exist ".\TLSim\inverse_park_sf.c" del ".\TLSim\inverse_park_sf.c" 
if exist ".\TLSim\inverse_park_pcf.c" del ".\TLSim\inverse_park_pcf.c" 
if exist ".\TLSim\inverse_park_frm.h" del ".\TLSim\inverse_park_frm.h" 
if exist ".\TLSim\globals_inverse_park.*" del ".\TLSim\globals_inverse_park.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\inverse_park\Metadata\inverse_park_SubsystemObject.dd" del ".\TLProj\inverse_park\Metadata\inverse_park_SubsystemObject.dd" 
if exist ".\TLProj\inverse_park" rmdir /s /q ".\TLProj\inverse_park" 
if exist ".\TLSim\StubCode\inverse_park" rmdir /s /q ".\TLSim\StubCode\inverse_park" 
if exist ".\TLProj\inverse_park\CGReport" rmdir /s /q ".\TLProj\inverse_park\CGReport" 
if exist ".\TLProj\inverse_park\Metadata" call:delete_if_empty ".\TLProj\inverse_park\Metadata"
if exist ".\TLProj\inverse_park" call:delete_if_empty ".\TLProj\inverse_park"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit svm
rem *********************************************************************************************


:delete_svm_files
set tl_subsys=svm
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\svm_fri.c" del ".\TLSim\svm_fri.c" 
if exist ".\TLSim\svm_fri.h" del ".\TLSim\svm_fri.h" 
if exist ".\TLSim\svm_sf.c" del ".\TLSim\svm_sf.c" 
if exist ".\TLSim\svm_pcf.c" del ".\TLSim\svm_pcf.c" 
if exist ".\TLSim\svm_frm.h" del ".\TLSim\svm_frm.h" 
if exist ".\TLSim\globals_svm.*" del ".\TLSim\globals_svm.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\svm\Metadata\svm_SubsystemObject.dd" del ".\TLProj\svm\Metadata\svm_SubsystemObject.dd" 
if exist ".\TLProj\svm" rmdir /s /q ".\TLProj\svm" 
if exist ".\TLSim\StubCode\svm" rmdir /s /q ".\TLSim\StubCode\svm" 
if exist ".\TLProj\svm\CGReport" rmdir /s /q ".\TLProj\svm\CGReport" 
if exist ".\TLProj\svm\Metadata" call:delete_if_empty ".\TLProj\svm\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\svm" call:delete_if_empty ".\TLProj\svm"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit elim_dc_bus_ripple
rem *********************************************************************************************


:delete_elim_dc_bus_ripple_files
set tl_subsys=elim_dc_bus_ripple
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\elim_dc_bus_ripple_fri.c" del ".\TLSim\elim_dc_bus_ripple_fri.c" 
if exist ".\TLSim\elim_dc_bus_ripple_fri.h" del ".\TLSim\elim_dc_bus_ripple_fri.h" 
if exist ".\TLSim\elim_dc_bus_ripple_sf.c" del ".\TLSim\elim_dc_bus_ripple_sf.c" 
if exist ".\TLSim\elim_dc_bus_ripple_pcf.c" del ".\TLSim\elim_dc_bus_ripple_pcf.c" 
if exist ".\TLSim\elim_dc_bus_ripple_frm.h" del ".\TLSim\elim_dc_bus_ripple_frm.h" 
if exist ".\TLSim\globals_elim_dc_bus_ripple.*" del ".\TLSim\globals_elim_dc_bus_ripple.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\elim_dc_bus_ripple\Metadata\elim_dc_bus_ripple_SubsystemObject.dd" del ".\TLProj\elim_dc_bus_ripple\Metadata\elim_dc_bus_ripple_SubsystemObject.dd" 
if exist ".\TLProj\elim_dc_bus_ripple" rmdir /s /q ".\TLProj\elim_dc_bus_ripple" 
if exist ".\TLSim\StubCode\elim_dc_bus_ripple" rmdir /s /q ".\TLSim\StubCode\elim_dc_bus_ripple" 
if exist ".\TLProj\elim_dc_bus_ripple\CGReport" rmdir /s /q ".\TLProj\elim_dc_bus_ripple\CGReport" 
if exist ".\TLProj\elim_dc_bus_ripple\Metadata" call:delete_if_empty ".\TLProj\elim_dc_bus_ripple\Metadata"
if exist ".\TLProj\elim_dc_bus_ripple" call:delete_if_empty ".\TLProj\elim_dc_bus_ripple"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit bemf_pid_gains_update
rem *********************************************************************************************


:delete_bemf_pid_gains_update_files
set tl_subsys=bemf_pid_gains_update
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\bemf_pid_gains_update_fri.c" del ".\TLSim\bemf_pid_gains_update_fri.c" 
if exist ".\TLSim\bemf_pid_gains_update_fri.h" del ".\TLSim\bemf_pid_gains_update_fri.h" 
if exist ".\TLSim\bemf_pid_gains_update_sf.c" del ".\TLSim\bemf_pid_gains_update_sf.c" 
if exist ".\TLSim\bemf_pid_gains_update_pcf.c" del ".\TLSim\bemf_pid_gains_update_pcf.c" 
if exist ".\TLSim\bemf_pid_gains_update_frm.h" del ".\TLSim\bemf_pid_gains_update_frm.h" 
if exist ".\TLSim\globals_bemf_pid_gains_update.*" del ".\TLSim\globals_bemf_pid_gains_update.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\bemf_pid_gains_update\Metadata\bemf_pid_gains_update_SubsystemObject.dd" del ".\TLProj\bemf_pid_gains_update\Metadata\bemf_pid_gains_update_SubsystemObject.dd" 
if exist ".\TLProj\bemf_pid_gains_update" rmdir /s /q ".\TLProj\bemf_pid_gains_update" 
if exist ".\TLSim\StubCode\bemf_pid_gains_update" rmdir /s /q ".\TLSim\StubCode\bemf_pid_gains_update" 
if exist ".\TLProj\bemf_pid_gains_update\CGReport" rmdir /s /q ".\TLProj\bemf_pid_gains_update\CGReport" 
if exist ".\TLProj\bemf_pid_gains_update\Metadata" call:delete_if_empty ".\TLProj\bemf_pid_gains_update\Metadata"
if exist ".\TLProj\bemf_pid_gains_update" call:delete_if_empty ".\TLProj\bemf_pid_gains_update"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit bemf_obsrv_gains_update
rem *********************************************************************************************


:delete_bemf_obsrv_gains_update_files
set tl_subsys=bemf_obsrv_gains_update
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\bemf_obsrv_gains_update_fri.c" del ".\TLSim\bemf_obsrv_gains_update_fri.c" 
if exist ".\TLSim\bemf_obsrv_gains_update_fri.h" del ".\TLSim\bemf_obsrv_gains_update_fri.h" 
if exist ".\TLSim\bemf_obsrv_gains_update_sf.c" del ".\TLSim\bemf_obsrv_gains_update_sf.c" 
if exist ".\TLSim\bemf_obsrv_gains_update_pcf.c" del ".\TLSim\bemf_obsrv_gains_update_pcf.c" 
if exist ".\TLSim\bemf_obsrv_gains_update_frm.h" del ".\TLSim\bemf_obsrv_gains_update_frm.h" 
if exist ".\TLSim\globals_bemf_obsrv_gains_update.*" del ".\TLSim\globals_bemf_obsrv_gains_update.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\bemf_obsrv_gains_update\Metadata\bemf_obsrv_gains_update_SubsystemObject.dd" del ".\TLProj\bemf_obsrv_gains_update\Metadata\bemf_obsrv_gains_update_SubsystemObject.dd" 
if exist ".\TLProj\bemf_obsrv_gains_update" rmdir /s /q ".\TLProj\bemf_obsrv_gains_update" 
if exist ".\TLSim\StubCode\bemf_obsrv_gains_update" rmdir /s /q ".\TLSim\StubCode\bemf_obsrv_gains_update" 
if exist ".\TLProj\bemf_obsrv_gains_update\CGReport" rmdir /s /q ".\TLProj\bemf_obsrv_gains_update\CGReport" 
if exist ".\TLProj\bemf_obsrv_gains_update\Metadata" call:delete_if_empty ".\TLProj\bemf_obsrv_gains_update\Metadata"
if exist ".\TLProj\bemf_obsrv_gains_update" call:delete_if_empty ".\TLProj\bemf_obsrv_gains_update"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit tracker_pid_gains_update
rem *********************************************************************************************


:delete_tracker_pid_gains_update_files
set tl_subsys=tracker_pid_gains_update
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\tracker_pid_gains_update_fri.c" del ".\TLSim\tracker_pid_gains_update_fri.c" 
if exist ".\TLSim\tracker_pid_gains_update_fri.h" del ".\TLSim\tracker_pid_gains_update_fri.h" 
if exist ".\TLSim\tracker_pid_gains_update_sf.c" del ".\TLSim\tracker_pid_gains_update_sf.c" 
if exist ".\TLSim\tracker_pid_gains_update_pcf.c" del ".\TLSim\tracker_pid_gains_update_pcf.c" 
if exist ".\TLSim\tracker_pid_gains_update_frm.h" del ".\TLSim\tracker_pid_gains_update_frm.h" 
if exist ".\TLSim\globals_tracker_pid_gains_update.*" del ".\TLSim\globals_tracker_pid_gains_update.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\tracker_pid_gains_update\Metadata\tracker_pid_gains_update_SubsystemObject.dd" del ".\TLProj\tracker_pid_gains_update\Metadata\tracker_pid_gains_update_SubsystemObject.dd" 
if exist ".\TLProj\tracker_pid_gains_update" rmdir /s /q ".\TLProj\tracker_pid_gains_update" 
if exist ".\TLSim\StubCode\tracker_pid_gains_update" rmdir /s /q ".\TLSim\StubCode\tracker_pid_gains_update" 
if exist ".\TLProj\tracker_pid_gains_update\CGReport" rmdir /s /q ".\TLProj\tracker_pid_gains_update\CGReport" 
if exist ".\TLProj\tracker_pid_gains_update\Metadata" call:delete_if_empty ".\TLProj\tracker_pid_gains_update\Metadata"
if exist ".\TLProj\tracker_pid_gains_update" call:delete_if_empty ".\TLProj\tracker_pid_gains_update"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit custom_observer_bemf_6step
rem *********************************************************************************************


:delete_custom_observer_bemf_6step_files
set tl_subsys=custom_observer_bemf_6step
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\custom_observer_bemf_6Step_fri.c" del ".\TLSim\custom_observer_bemf_6Step_fri.c" 
if exist ".\TLSim\custom_observer_bemf_6Step_fri.h" del ".\TLSim\custom_observer_bemf_6Step_fri.h" 
if exist ".\TLSim\custom_observer_bemf_6step_sf.c" del ".\TLSim\custom_observer_bemf_6step_sf.c" 
if exist ".\TLSim\custom_observer_bemf_6step_pcf.c" del ".\TLSim\custom_observer_bemf_6step_pcf.c" 
if exist ".\TLSim\custom_observer_bemf_6step_frm.h" del ".\TLSim\custom_observer_bemf_6step_frm.h" 
if exist ".\TLSim\globals_custom_observer_bemf_6step.*" del ".\TLSim\globals_custom_observer_bemf_6step.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\custom_observer_bemf_6Step\Metadata\custom_observer_bemf_6step_SubsystemObject.dd" del ".\TLProj\custom_observer_bemf_6Step\Metadata\custom_observer_bemf_6step_SubsystemObject.dd" 
if exist ".\TLProj\custom_observer_bemf_6Step" rmdir /s /q ".\TLProj\custom_observer_bemf_6Step" 
if exist ".\TLSim\StubCode\custom_observer_bemf_6Step" rmdir /s /q ".\TLSim\StubCode\custom_observer_bemf_6Step" 
if exist ".\TLProj\custom_observer_bemf_6Step\CGReport" rmdir /s /q ".\TLProj\custom_observer_bemf_6Step\CGReport" 
if exist ".\TLProj\custom_observer_bemf_6Step\Metadata" call:delete_if_empty ".\TLProj\custom_observer_bemf_6Step\Metadata"
if exist ".\TLProj\custom_observer_bemf_6Step" call:delete_if_empty ".\TLProj\custom_observer_bemf_6Step"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit observer_bhspin_lib
rem *********************************************************************************************


:delete_observer_bhspin_lib_files
set tl_subsys=observer_bhspin_lib
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\observer_bhspin_lib_fri.c" del ".\TLSim\observer_bhspin_lib_fri.c" 
if exist ".\TLSim\observer_bhspin_lib_fri.h" del ".\TLSim\observer_bhspin_lib_fri.h" 
if exist ".\TLSim\observer_bhspin_lib_sf.c" del ".\TLSim\observer_bhspin_lib_sf.c" 
if exist ".\TLSim\observer_bhspin_lib_pcf.c" del ".\TLSim\observer_bhspin_lib_pcf.c" 
if exist ".\TLSim\observer_bhspin_lib_frm.h" del ".\TLSim\observer_bhspin_lib_frm.h" 
if exist ".\TLSim\globals_observer_bhspin_lib.*" del ".\TLSim\globals_observer_bhspin_lib.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\observer_bhspin_lib\Metadata\observer_bhspin_lib_SubsystemObject.dd" del ".\TLProj\observer_bhspin_lib\Metadata\observer_bhspin_lib_SubsystemObject.dd" 
if exist ".\TLProj\observer_bhspin_lib" rmdir /s /q ".\TLProj\observer_bhspin_lib" 
if exist ".\TLSim\StubCode\observer_bhspin_lib" rmdir /s /q ".\TLSim\StubCode\observer_bhspin_lib" 
if exist ".\TLProj\observer_bhspin_lib\CGReport" rmdir /s /q ".\TLProj\observer_bhspin_lib\CGReport" 
if exist ".\TLProj\observer_bhspin_lib\Metadata" call:delete_if_empty ".\TLProj\observer_bhspin_lib\Metadata"
if exist ".\TLProj\observer_bhspin_lib" call:delete_if_empty ".\TLProj\observer_bhspin_lib"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit mil
rem *********************************************************************************************


:delete_mil_files
set tl_subsys=mil
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\MIL_fri.c" del ".\TLSim\MIL_fri.c" 
if exist ".\TLSim\MIL_fri.h" del ".\TLSim\MIL_fri.h" 
if exist ".\TLSim\mil_sf.c" del ".\TLSim\mil_sf.c" 
if exist ".\TLSim\mil_pcf.c" del ".\TLSim\mil_pcf.c" 
if exist ".\TLSim\mil_frm.h" del ".\TLSim\mil_frm.h" 
if exist ".\TLSim\globals_mil.*" del ".\TLSim\globals_mil.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\MIL\Metadata\mil_SubsystemObject.dd" del ".\TLProj\MIL\Metadata\mil_SubsystemObject.dd" 
if exist ".\TLProj\MIL" rmdir /s /q ".\TLProj\MIL" 
if exist ".\TLSim\StubCode\MIL" rmdir /s /q ".\TLSim\StubCode\MIL" 
if exist ".\TLProj\MIL\CGReport" rmdir /s /q ".\TLProj\MIL\CGReport" 
if exist ".\TLProj\MIL\Metadata" call:delete_if_empty ".\TLProj\MIL\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\MIL" call:delete_if_empty ".\TLProj\MIL"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit pid
rem *********************************************************************************************


:delete_pid_files
set tl_subsys=pid
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\PID_fri.c" del ".\TLSim\PID_fri.c" 
if exist ".\TLSim\PID_fri.h" del ".\TLSim\PID_fri.h" 
if exist ".\TLSim\pid_sf.c" del ".\TLSim\pid_sf.c" 
if exist ".\TLSim\pid_pcf.c" del ".\TLSim\pid_pcf.c" 
if exist ".\TLSim\pid_frm.h" del ".\TLSim\pid_frm.h" 
if exist ".\TLSim\globals_pid.*" del ".\TLSim\globals_pid.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\PID\Metadata\pid_SubsystemObject.dd" del ".\TLProj\PID\Metadata\pid_SubsystemObject.dd" 
if exist ".\TLProj\PID" rmdir /s /q ".\TLProj\PID" 
if exist ".\TLSim\StubCode\PID" rmdir /s /q ".\TLSim\StubCode\PID" 
if exist ".\TLProj\PID\CGReport" rmdir /s /q ".\TLProj\PID\CGReport" 
if exist ".\TLProj\PID\Metadata" call:delete_if_empty ".\TLProj\PID\Metadata"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj\PID" call:delete_if_empty ".\TLProj\PID"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit gain_scheduling
rem *********************************************************************************************


:delete_gain_scheduling_files
set tl_subsys=gain_scheduling
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\Gain_Scheduling_fri.c" del ".\TLSim\Gain_Scheduling_fri.c" 
if exist ".\TLSim\Gain_Scheduling_fri.h" del ".\TLSim\Gain_Scheduling_fri.h" 
if exist ".\TLSim\gain_scheduling_sf.c" del ".\TLSim\gain_scheduling_sf.c" 
if exist ".\TLSim\gain_scheduling_pcf.c" del ".\TLSim\gain_scheduling_pcf.c" 
if exist ".\TLSim\gain_scheduling_frm.h" del ".\TLSim\gain_scheduling_frm.h" 
if exist ".\TLSim\globals_gain_scheduling.*" del ".\TLSim\globals_gain_scheduling.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\Gain_Scheduling\Metadata\gain_scheduling_SubsystemObject.dd" del ".\TLProj\Gain_Scheduling\Metadata\gain_scheduling_SubsystemObject.dd" 
if exist ".\TLProj\Gain_Scheduling" rmdir /s /q ".\TLProj\Gain_Scheduling" 
if exist ".\TLSim\StubCode\Gain_Scheduling" rmdir /s /q ".\TLSim\StubCode\Gain_Scheduling" 
if exist ".\TLProj\Gain_Scheduling\CGReport" rmdir /s /q ".\TLProj\Gain_Scheduling\CGReport" 
if exist ".\TLProj\Gain_Scheduling\Metadata" call:delete_if_empty ".\TLProj\Gain_Scheduling\Metadata"
if exist ".\TLProj\Gain_Scheduling" call:delete_if_empty ".\TLProj\Gain_Scheduling"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)



rem *********************************************************************************************
rem ** Cleanup file related to code generation unit controller
rem *********************************************************************************************


:delete_controller_files
set tl_subsys=controller
if exist %tl_subsys%_sf.c        del %tl_subsys%_sf.c
if exist %tl_subsys%_sf.mexw64   del %tl_subsys%_sf.mexw64
if exist %tl_subsys%_sfcn.c      del %tl_subsys%_sfcn.c
if exist %tl_subsys%_sfcn.mexw64 del %tl_subsys%_sfcn.mexw64
if exist %tl_subsys%_sf*.pdb     del %tl_subsys%_sf*.pdb
if exist %tl_subsys%_sf.ilk      del %tl_subsys%_sf.ilk

if exist ".\TLSim\Controller_fri.c" del ".\TLSim\Controller_fri.c" 
if exist ".\TLSim\Controller_fri.h" del ".\TLSim\Controller_fri.h" 
if exist ".\TLSim\controller_sf.c" del ".\TLSim\controller_sf.c" 
if exist ".\TLSim\controller_pcf.c" del ".\TLSim\controller_pcf.c" 
if exist ".\TLSim\controller_frm.h" del ".\TLSim\controller_frm.h" 
if exist ".\TLSim\globals_controller.*" del ".\TLSim\globals_controller.*" 
if exist ".\TLSim\tlsim_*_globals.c" del ".\TLSim\tlsim_*_globals.c" 
if exist ".\TLSim\tlsim_*_enums.c" del ".\TLSim\tlsim_*_enums.c" 
if exist ".\TLProj\Controller\Metadata\controller_SubsystemObject.dd" del ".\TLProj\Controller\Metadata\controller_SubsystemObject.dd" 
if exist ".\TLProj\Controller" rmdir /s /q ".\TLProj\Controller" 
if exist ".\TLSim\StubCode\Controller" rmdir /s /q ".\TLSim\StubCode\Controller" 
if exist ".\TLProj\Controller\CGReport" rmdir /s /q ".\TLProj\Controller\CGReport" 
if exist ".\TLProj\Controller\Metadata" call:delete_if_empty ".\TLProj\Controller\Metadata"
if exist ".\TLProj\Controller" call:delete_if_empty ".\TLProj\Controller"
if exist ".\TLSim\StubCode" call:delete_if_empty ".\TLSim\StubCode"
if exist ".\TLProj" call:delete_if_empty ".\TLProj"
if exist ".\TLSim" call:delete_if_empty ".\TLSim"
GOTO :EOF
)

:delete_if_empty
set _TMP=
for /f "delims=" %%a in ('dir /b %1') do set _TMP="%%a"
IF {%_TMP%}=={} (
      set _empty=Empty
      if exist %1 rmdir /s /q %1
)
