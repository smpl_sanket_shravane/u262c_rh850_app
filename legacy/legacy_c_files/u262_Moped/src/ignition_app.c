/****************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: ignition_app.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 12.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are specific to the ignition application.
*
* Change history
*
*  +------------+------------------------------------------------------------+-----------+
*  |    Date    |                Changelog                                   | Author    |
*  +------------+------------------------------------------------------------+-----------+
*  | 19 Dec 16  | Added libinterp-2.0 combatible interpolation functions     | Anay      |
*  +------------+------------------------------------------------------------+-----------+
*  | 13 Feb 17  | Improvement in dwell time exceution                        | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 20 Mar 17  | Function added for Pip width measurement                   | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 22 Mar 17  | Changed for acceleration and deacceleraton logic           | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 03 Apr 17  | Updated for corrective map logic                           | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 10 Apr 17  | Added logic for secondary spark till corrective map timing | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 12 Apr 17  | Added logic for rpm based dwell change in batteryless      | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 12 May 17  | Added logic in advance interrupt for angle fluctuation due | Sandeep   |
*  |            | to other hight priority interrupt. Also accounted for      |           |
*  |            | hardware offset.                                           |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 23 May 17  | Added logic for high rpm detection issue and cps fault     | Sandeep   |
*  +------------+------------------------------------------------------------+-----------+
*  | 13 Nov 17  | Corrective map execution modified as per U162              | Mahesh    |
*  +------------+------------------------------------------------------------+-----------+
*  | 21 Nov 17  | Routine to decide batteryless dwell time removed as per    | Mahesh    |
*  |            | TVSM specifications dated. 21 Nov 17                       |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 08 Dec 17  | Bugfix: Logic of hysteresis for switching to HW_TRG mode   | Mahesh    |
*  |            | from corrective map is modified                            |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 19 Dec 17  | Bugfix: Engine speed detecetd as 150 though engine is OFF  | Mahesh    |
*  |            |  nOveflowsSinceLastZero reseted in zero pulse ISR          |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 22 Dec 17  | 'nOverflowForCorrectiveAngle' is reset if engine speed is  | Mahesh    |
*  |            | decreased below CAL_HW_TRG_MIN_RPM for CAL_N_ENGINE_REV_DWN|           |
*  |            | and CAL_CORRECTIVE_CURVE_TIME_SEC is not elapsed           |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 05 Jan 18  | Bugfix: Corrective map is executing even after corrective  | Mahesh    |
*  |            | time elapsed                                               |           |
*  +------------+------------------------------------------------------------+-----------+
*  | 13 Mar 17  | Variable angle corrective map strategy as per TVSM req.    | Mahesh    |
*  +------------+------------------------------------------------------------+-----------+
*  | 17 Mar 18  | Bugfix: During correcive map execution if engine speed     | Mahesh    |
*  |            | drops below CAL_HW_TRG_MIN, dwell start is HW triggered    |           |
*  |            | and end is computed. Secondary dwell starts post zero pulse|           |
*  +------------+------------------------------------------------------------+-----------+
*  | 31 July 18 | Modify the software for MBSD                               |  Kashif   |
*  +------------+------------------------------------------------------------+-----------+
****************************************************************************************************************/
#include "ignition_macro.h"
#include "ignition_app.h"
#include "load_computation.h"
#include "ignition_common.h"
#include "interp.h"
#include "ign_params.h"

#define IGN_TIM_RESOLUTION   1  /* defines timer resolution in uS */
#define COUNT_CORRECTIVE_MAP (((uint32_t)CAL_CORRECTIVE_CURVE_TIME_SEC*1000)/65)
#define COUNT_DECELERATION_MAP (((uint32_t)CAL_DEACC_N_SEC_ENABLE*1000)/65)
#define COUNT_PA_DECELERATION_MAP (((uint32_t)CAL_PA_DEACC_N_SEC_ENABLE*1000)/65)
#define COUNT_FIRST_CYCLE 400000 /* define counts of 150 rpm */
#define COUNT_CPS_FAULT 20  /* No of counts for CPS fault detection */
#define HW_OFFSET_RPM_TH  6000
#define MAX_TIMER_COUNTS 65536
#define DWELL_COUNT_OFFSET 20
#define CAL_VARIABLE_ENGINE_SPEED 1200

IGNITION_t gIgnition;
UNIFORM_MAP_S8_t IgnitionTimingMap;
UNIFORM_CURVE_S8_t CorrectiveAngleCurve;
UNIFORM_CURVE_S16_t AccelerationCurve;
UNIFORM_CURVE_S16_t DeaccelerationCurve;
UNIFORM_CURVE_S16_t FullLoadCountsCurve;

typedef enum
{
    CPS_LAST_STATE_NONE = 0,
    CPS_LAST_STATE_ADV = 1,
    CPS_LAST_STATE_ZERO = 2,
}CPS_LAST_STATE_t;

CPS_LAST_STATE_t eCpsLastState = CPS_LAST_STATE_NONE;

typedef enum
{
    ENG_SPEED_TREND_UNKNOWN = 0,
    ENG_SPEED_TREND_INCREASING = 1,
    ENG_SPEED_TREND_DECREASING = 2,
    ENG_SPEED_TREND_STABLE = 3,
} ENG_SPEED_TREND_t;

ENG_SPEED_TREND_t eEngSpeedTrend, ePwrAsstEngSpeedTrend;


static void prvFixedDwellComputation(void);
static void prvScheduleIgnTimerA(uint16_t timerACount);
static void prvScheduleIgnTimerB(uint16_t timerBCount);
static void prvEngineSpeedTrendDetect(void);
static void prvPwrAsstEngineSpeedTrendDetect(void);
static void prvAccelerationDetection(void);
static void prvPwrAsstAccelerationDetection(void);
static void prvDeaccelerationDetection(void);
static void prvPwrAsstDeaccelerationDetection(void);
static void prvIdleStability(void);
static void prvResetAccelerationVariables(void);
static void prvResetDeaccelerationVariables(void);
static void prvPwrAsstResetAccelerationVariables(void);
static void prvPwrAsstResetDeaccelerationVariables(void);
static void prvResetCorrectiveMapVariables(void);
static void prvExtendedDwellCheck(void);
static void prvTurnOnPrimaryIgbt(void);
static void prvTurnOffPrimaryIgbt(void);
static void prvResetIgnitionVariables(void);
static void prvDecideDigitalFiring(uint16_t Engine_rpm, uint16_t AnalogFiring_rpm);
static uint8_t prvComputePipWidth(uint32_t DiffAdvToZero, uint32_t DiffZeroToZero);
static void prvCheckPulserCoilInterchanged(bool bFirstPulse);
static bool prvDecideCorrectiveMapExecution(uint16_t Engine_rpm);

const uint16_t CAL_FullLoadCountsCurve[] = {5320, 5320, 5320, 5320, 5320, 3990, 3155, 2526, 2074, 1742, 1441, 1139, 894, 705, 574, 474, 399, 339, 290, 251, 220, 194, 172, 151, 132, 116, 104, 92, 82, 74, 67, 61, 54, 48, 42, 36};
const uint8_t CAL_SPARK_HW_OFFSET1 = 40;
const uint8_t CAL_SPARK_HW_OFFSET2 = 65;

/* Minimum Start RPM For Ignition timing table */
const uint16_t CAL_IGN_MAP_START_RPM = 1000;
/* Maximum End RPM For Ignition timing table */
const uint16_t CAL_IGN_MAP_END_RPM = 10000;
/* IGNITION TIMING TABLE INDEX */
const uint16_t CAL_IGN_MAP_MAX_ROWS = 45;
/* IGNITION TIMING TABLE RPM STEP */
const uint16_t CAL_IGN_MAP_RPM_STEP = 200;
/* NORMALIZATION FACTOR */
const uint16_t CAL_IGN_TABLE_INTERP_RPM_NORM_FAC = 20;
/*Load filter is bypassed if difference between raw load and Filtered load is more than DELTA_FAST_WOT */
const uint8_t CAL_FAST_WOT_DELTA_LOAD = 40;
/*Load filter is bypassed if for N_CYCLE_FAST_WOT difference between raw load and Filtered load is more than CAL_FAST_WOT_DELTA_LOAD */
const uint8_t N_CYCLES_FAST_WOT = 2;

const uint16_t CAL_DEACC_CURVE_START_RPM = 600;
const uint8_t CAL_DEACC_CURVE_RPM_STEP = 50;
const uint8_t CAL_DEACC_CURVE_MAX_ROWS = 69;
const uint16_t CAL_DEACC_CURVE_END_RPM = 4000;
const uint16_t RPM_FIXED_DWELL = 5000;
const uint16_t DWELL_RPM_TH_1 = 5900;

static uint16_t LoadValueFilt_percent;

bool bAdvPulseReceived = false;
bool bZeroPulseReceived = false;
bool bFirstCrankPulse = false;

/*Added to avoid secondary dwell start after zero pulse received. dt. 17-Mar-18*/
static volatile bool bZeroReceived = false;

static volatile uint32_t tDiffAdvToZero, tDiffZeroToZero;
static volatile uint32_t tPrevDiffZeroToZero;
static uint32_t tExtendedTimer = 0, tExtendedZeroPulse = 0, tPreExtendedZeroPulse = 0, tExtendedAdvPulse = 0;

static volatile bool bJustDisabled = false;
static volatile uint32_t tPrevDiffAdvToZero = 0;
static volatile uint16_t tAdvToIgnition = 0;
static volatile uint16_t tZeroToDwellStart = 0;
static volatile uint16_t tDwellTime;
static uint16_t tCapturedZeroPulse = 0, tCapturedIgnCounter = 0;

static int16_t IgnTimingToExecute_deg = 5;
static uint8_t nOveflowsSinceLastZero = 0;
static uint32_t tComputedRawLoad = 0;
static volatile uint16_t LoadValueRaw_percent = 0;
bool bStartDelayedDwell = false, bDelayedDwellStartEnable = false;
static uint16_t tAdvToDwellStart = 0,IgnitionTimingFromAdv_deg = 0;
static volatile uint16_t PrevEngineSpeed_rpm = 0;
static volatile uint16_t nDeaccEnableAfterCrank = 0;
static volatile uint16_t nPwrAsstDeaccEnableAfterCrank = 0;
bool bAccelerationCurveEnabled = false;
bool bDeaccelerationCurveEnabled = false;
bool bPwrAsstAccelerationEnabled = false;
bool bPwrAsstDeaccelerationEnabled = false;
static volatile int16_t AccelerationAngle_deg = 0,DecelerationAngle_deg = 0;
bool bExtendDwell = false;
bool bSparkCutoff = false;
bool bEngineCranked = false;
bool bStartDwellAtAdvPulse = true;
bool bEndDwellAtZeroPulse = true;
bool bDwellStartInitiated = false;
bool bDwellEndInitiated = false;
static uint32_t tFullLoadValue = 0;
static uint16_t nOverflowForCorrectiveAngle = 0;
static int8_t CorrectiveAngle_deg = 0;
/* Secondary Spark Variables -Start*/
static bool bSecondarySparkEnable = false;
static bool bSecDwellStartInitiated = false;
static bool bSecDwellEndInitiated = false;
volatile bool bCorrectiveMapEnabled = false;
static uint8_t nEngineRevUp = 0;
static uint8_t nEngineRevDwn = 0;
static volatile bool bCorrectiveToHwTrg = false;  /*Added for cyclic hysteresis of Corrective to Hw_Trg mode*/
/*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
static volatile bool bCorrectiveExeTimeElapsed = false;  /*Declare corrective execution time is not elapsed*/
static uint8_t nEngineRev = 0;
static uint16_t DeltaIgnAngle_deg = 0;
static uint8_t nVariableAngle = 0;
static uint8_t bVariableAngle = true;
/* Secondary Spark Variables -End*/
static uint16_t tCapturedAdvPulse = 0;
uint8_t SparkHardwareOffset = 40;


/* Variables for pulser coil interchanged fault */
static volatile uint32_t tDiffZeroToAdv = 0;
bool bPulserCoilInterchanged = false;
static char nPulserFault = 0;

int8_t gPAAccInc = 0;
int8_t gPADEAccInc = 0;
int8_t gPAStableRpm = 0;

/*****************************************************************************************
function          : Ignition_Enable
description       : This function enables the ignition.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_Enable(void)
{
    gIgnition.bEnabled = true;
}


/*****************************************************************************************
function          : Ignition_Disable
description       : This function disables the ignition.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_Disable(void)
{
    gIgnition.bEnabled = false;
    bJustDisabled = true;
}

void Ignition_HardDisable(void)
{
    TURN_OFF_PRI_IGBT();
    gIgnition.bEnabled = false;
    bJustDisabled = true;
}


/*****************************************************************************************
function          : Ignition_RaiseExcessCurrentFault
description       : This function sets the excess current fault.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_RaiseExcessCurrentFault(void)
{
    gIgnition.bExcessCurrentDetected = true;
    prvTurnOffPrimaryIgbt();
}


/*****************************************************************************************
function          : Ignition_ClearExcessCurrentFault
description       : This function clears the excess current fault.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_ClearExcessCurrentFault(void)
{
    gIgnition.bExcessCurrentDetected = false;
}


/*****************************************************************************************
function          : Ignition_Init
description       : This function initializes ignition variables.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_Init(void)
{

    gIgnition.EngineSpeed_rpm = 0;
    gIgnition.bExcessCurrentDetected = false;
    gIgnition.bEngineOn = false;

    IgnitionTimingMap.StartY = CAL_IGN_MAP_START_RPM;
    IgnitionTimingMap.StepY = CAL_IGN_MAP_RPM_STEP;
    IgnitionTimingMap.LengthY = CAL_IGN_MAP_MAX_ROWS;
    IgnitionTimingMap.StartX = 0;
    IgnitionTimingMap.StepX = CAL_IGN_MAP_LOAD_STEP;
    IgnitionTimingMap.LengthX = 11;
    IgnitionTimingMap.pTableStart = &CAL_IgnTimingMap[0][0];

    AccelerationCurve.Start = CAL_IGN_MAP_START_RPM;
    AccelerationCurve.Step = CAL_IGN_MAP_RPM_STEP;
    AccelerationCurve.Length = CAL_IGN_MAP_MAX_ROWS;
    AccelerationCurve.End = CAL_IGN_MAP_END_RPM;
    AccelerationCurve.pTableStart = &CAL_AccelerationCurve[0];

    DeaccelerationCurve.Start = CAL_DEACC_CURVE_START_RPM;
    DeaccelerationCurve.Step = CAL_DEACC_CURVE_RPM_STEP;
    DeaccelerationCurve.Length = CAL_DEACC_CURVE_MAX_ROWS;
    DeaccelerationCurve.End = CAL_DEACC_CURVE_END_RPM;
    DeaccelerationCurve.pTableStart = &CAL_DeaccelerationCurve[0];

    FullLoadCountsCurve.Start = CAL_IGN_MAP_START_RPM;
    FullLoadCountsCurve.Step = CAL_IGN_MAP_RPM_STEP;
    FullLoadCountsCurve.Length = CAL_IGN_MAP_MAX_ROWS;
    FullLoadCountsCurve.End = CAL_IGN_MAP_END_RPM;
    FullLoadCountsCurve.pTableStart = &CAL_FullLoadCountsCurve[0];

    CorrectiveAngleCurve.Start = CAL_IGN_MAP_START_RPM;
    CorrectiveAngleCurve.Step = CAL_IGN_MAP_RPM_STEP;
    CorrectiveAngleCurve.Length = CAL_IGN_MAP_MAX_ROWS;
    CorrectiveAngleCurve.End = CAL_IGN_MAP_END_RPM;
    CorrectiveAngleCurve.pTableStart = &CAL_CorrectiveAngleCurve[0];

    /*Dwell time for Battery and Batteryless operation*/
    tDwellTime = CAL_DWELL_TIME;

}


/*****************************************************************************************
function          : Ignition_Reset
description       : This function resets the ignition variables.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_Reset(void)
{
    gIgnition.bEngineOn = false;
    prvTurnOffPrimaryIgbt();
    prvResetIgnitionVariables();
    ResetLoadVariables();
}


/*********************************************************************************************************************************************
function          : Ignition_MainLoopHandler
description       : This function handles all the ignition computations such as Engine Speed,
                    load computation, dwell exceution, ignition timing interpolatioon, etc.
                    It also hanndles all the supporting functions specific to the application.
Input parameters  : None
Output parameters : None
Return value      : None
*********************************************************************************************************************************************/
void Ignition_MainLoopHandler(uint16_t *u16Data, uint8_t *u8Data, int16_t *i16Data)
{  
    MEAS_PA_ACC_FLAG = bPwrAsstAccelerationEnabled ;
    MEAS_PA_DEACC_FLAG = bPwrAsstDeaccelerationEnabled;
    MEAS_CPS_RPM = gIgnition.EngineSpeed_rpm;
    MEAS_ENGINE_LOAD = LoadValueFilt_percent;
    MEAS_IGN_TIMING = IgnTimingToExecute_deg;

    u16Data[0] =  MEAS_CPS_RPM;
    u16Data[1] =  MEAS_ENGINE_LOAD;
    u8Data[0]  =  MEAS_PIP_WIDTH ;
    u8Data[1]  =  MEAS_PA_ACC_FLAG;
    u8Data[2]  =  MEAS_PA_DEACC_FLAG;
   *i16Data    =  MEAS_IGN_TIMING;

    if((bZeroPulseReceived) && (!bDwellEndInitiated))
    {
        /* Variables saturated to fixed value for high rpm detection issue in first crank cycle */
    	if(bFirstCrankPulse == true)
    	{
    		tDiffZeroToZero = COUNT_FIRST_CYCLE;
    		tPrevDiffZeroToZero = COUNT_FIRST_CYCLE;
    		tPrevDiffAdvToZero = COUNT_FIRST_CYCLE;
    	}
        else
        {
            /*Nothing to do here*/
        }

       /*Compute engine speed*/
        gIgnition.EngineSpeed_rpm = ComputeEngineSpeed(tDiffZeroToZero, tPrevDiffZeroToZero, &bFirstCrankPulse, IGN_TIM_RESOLUTION);

     //   Cranking_ZeroPulseIsr2();


        /*Decide corrective Map execution based on RPM and Time*/
        bCorrectiveMapEnabled = prvDecideCorrectiveMapExecution(gIgnition.EngineSpeed_rpm);

        /*Enable secondary spark if corrective map is enabled*/
        if (bCorrectiveMapEnabled)
        {
            bSecondarySparkEnable = CAL_SEC_SPARK_ENABLE;
        }
        else
        {
            bSecondarySparkEnable = false;
        }


        /*Check if engine speed is stable or increasing or decreasing*/
        prvEngineSpeedTrendDetect();
        prvPwrAsstEngineSpeedTrendDetect();


        /*Check if Engine is cranked or not*/
        CheckEngineCranked(gIgnition.EngineSpeed_rpm, 150, &bEngineCranked, (CAL_COUNT_IGN_HW_TRIGGER + 1));

        /*Compute pipwidth*/
        MEAS_PIP_WIDTH = prvComputePipWidth(tDiffAdvToZero,tDiffZeroToZero);

		/*Stroke detection algotithn*/
        StrokeDetectionAlgo1(tDiffZeroToZero, tPrevDiffZeroToZero);

        /*Compute raw load*/
        tComputedRawLoad = ComputeRawLoadAlgo1(tDiffZeroToZero, tPrevDiffZeroToZero);

		/*Disable deceleration if Engine speed drops below CAL_DEACC_LOWER_RPM*/
        if(gIgnition.EngineSpeed_rpm <= CAL_DEACC_LOWER_RPM)
        {
            bDeaccelerationCurveEnabled = false;
        }
        else
        {
            /*Nothing to do here*/
        }


        /*Disable deceleration if Engine speed drops below CAL_DEACC_LOWER_RPM*/
        if(gIgnition.EngineSpeed_rpm <= CAL_PA_DEACC_LOWER_RPM)
        {
            bPwrAsstDeaccelerationEnabled = false;
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Decide Digital firing based on hardware trigger RPM*/
        if(bCorrectiveMapEnabled)
        {   /*bCorrectiveToHwTrg added to maintain cyclic hysteresis during corrective to HW_TRG transition*/
    	    if(bCorrectiveToHwTrg == true)
    	    {
    	       prvDecideDigitalFiring(gIgnition.EngineSpeed_rpm, CAL_RPM_HW_TRG_MIN);
    	    }
    	    else
    	    {
    		      /*Nothing to do here*/
	        }
        }
        else
        {
            prvDecideDigitalFiring(gIgnition.EngineSpeed_rpm, CAL_RPM_HW_TRG_MAX);
        }

        /*If decelration is enabled and engine speed drops below HW_TRG_MAX then execute computed dwell*/
        if((CAL_DEACCELARTION_DIGITAL_FIRING) && (bDeaccelerationCurveEnabled))
        {
            bStartDwellAtAdvPulse = false;
            bEndDwellAtZeroPulse = false;
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Check if engine speed exceeded spark cutoff limit*/
        CutOffIgnition(gIgnition.EngineSpeed_rpm, CAL_SPARK_CUTOFF_RPM, CAL_SPARK_CUTOFF_RPM, &bSparkCutoff, 2);

        /*Acceleration detection algorithm*/
        prvAccelerationDetection();
        prvPwrAsstAccelerationDetection();

        /*Deceleration detection algorithm*/
        prvDeaccelerationDetection();
        prvPwrAsstDeaccelerationDetection();

        /*Retain engine speed for further calculations*/
        PrevEngineSpeed_rpm = gIgnition.EngineSpeed_rpm;

        /*Compute Ignition angle to execute*/
        if(gIgnition.EngineSpeed_rpm < CAL_LOAD_DETECTION_MAX_RPM)
        {
            tFullLoadValue = UniformInterpCurve_S16(&FullLoadCountsCurve, gIgnition.EngineSpeed_rpm);
            LoadValueRaw_percent = NormalizeRawLoad(tFullLoadValue, tComputedRawLoad);
            SaturateFiltDuringFastWOT(LoadValueRaw_percent, LoadValueFilt_percent, CAL_FAST_WOT_DELTA_LOAD, CAL_MAX_LOAD_FAST_WOT, N_CYCLES_FAST_WOT);
            LoadValueFilt_percent = LoadDigitalFilter(LoadValueRaw_percent, CAL_LOAD_FILTER_COEFF);
            IgnTimingToExecute_deg = UniformInterpMap_S8(&IgnitionTimingMap, (int16_t)LoadValueFilt_percent, (int16_t)gIgnition.EngineSpeed_rpm);
        }
        else
        {
            LoadValueFilt_percent = 100;
            IgnTimingToExecute_deg = UniformInterpMap_S8(&IgnitionTimingMap, LoadValueFilt_percent, gIgnition.EngineSpeed_rpm);
        }

        /*Check if corrective map is enabled*/
        if(bCorrectiveMapEnabled)
        {
            CorrectiveAngle_deg = UniformInterpCurve_S8(&CorrectiveAngleCurve,gIgnition.EngineSpeed_rpm);

            /*Check if variable corrective angle flag not reset*/
            if(bVariableAngle == true)
            {
                /*Check if engine speed is less than 1200 RPM*/
                if(gIgnition.EngineSpeed_rpm <= CAL_VARIABLE_ENGINE_SPEED)
                {
                    /*Increment engine rev counter*/
                    nEngineRev++;
                    /*Reset variable angle counter*/
                    nVariableAngle = 0;

                    /*check  engine rev counter exceeds CAL_N_ENGINE_REV*/
                    if(nEngineRev >= CAL_N_ENGINE_REV)
                    {
                        /*Reset engine rev counter*/
                        nEngineRev = 0;
                        /*Increment DeltaIgnAngle_deg*/
                        DeltaIgnAngle_deg++;
                    }

                    /*Update corrective angle*/
                    //CorrectiveAngle_deg += DeltaIgnAngle_deg;

                    /*Check if corrective angle exceeds 15 degrees*/
                    if(DeltaIgnAngle_deg >= 5)
                    {
                        /*Saturate to 15 degrees*/
                        DeltaIgnAngle_deg = 5;
                    }
                }
                else
                {
                    /*Increment counter to count no of engine rev*/
                    nVariableAngle++;

                    /*Reset engine rev counter*/
                    nEngineRev = 0;

                    /*If engine revolution exceeds CAL_N_ENGINE_REV*/
                    if(nVariableAngle >= CAL_N_ENGINE_REV)
                    {
                        /*Disable strategy*/
                        bVariableAngle = false;
                        /*Saturate counter*/
                        nVariableAngle = CAL_N_ENGINE_REV + 1;
                    }
                }
            }

            /*Check if engine speed is less than 2000*/
            if(gIgnition.EngineSpeed_rpm < 2000)
            {
                IgnTimingToExecute_deg = CorrectiveAngle_deg + IgnTimingToExecute_deg + DeltaIgnAngle_deg;
            }
            else
            {
                IgnTimingToExecute_deg = CorrectiveAngle_deg + IgnTimingToExecute_deg;
            }

        }
        else
        {
            bSecondarySparkEnable = false;
        }

        /*Check if Decelration or Acceleration is enabled*/
        if(bDeaccelerationCurveEnabled)
        {
            IgnTimingToExecute_deg = UniformInterpCurve_S16(&DeaccelerationCurve, gIgnition.EngineSpeed_rpm);
        }
        else if(bAccelerationCurveEnabled)
        {
            IgnTimingToExecute_deg = UniformInterpCurve_S16(&AccelerationCurve, gIgnition.EngineSpeed_rpm);
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Saturate to MAX value, if igniion angle is exceeding PIPBASE + PIPWIDTH degrees*/
        if (IgnTimingToExecute_deg >= (CAL_PIP_WIDTH_DEG + CAL_PIP_BASE_DEG))
        {
	       IgnTimingToExecute_deg = (CAL_PIP_WIDTH_DEG + CAL_PIP_BASE_DEG);
        }
        else
        {
            /*Nothing to do here*/
        }

        /* used to compensate computational and hardware delay based on rpm */
        if(gIgnition.EngineSpeed_rpm < HW_OFFSET_RPM_TH)
        {
            SparkHardwareOffset = CAL_SPARK_HW_OFFSET1;
        }
        else
        {
            SparkHardwareOffset = CAL_SPARK_HW_OFFSET2;
        }

        /*Compute Advance to Ignition timer counts*/
        tAdvToIgnition = ComputeAdvToIgnTime(IgnTimingToExecute_deg, tPrevDiffAdvToZero, SparkHardwareOffset);

        /*Check if Igniton timing to execute is matching with PIPBASE*/
        if((IgnTimingToExecute_deg == CAL_PIP_BASE_DEG) && (gIgnition.EngineSpeed_rpm < RPM_FIXED_DWELL))
        {
            bEndDwellAtZeroPulse = true;
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Disable secondary spark if ignition angle is less than or equal to Base*/
        if(IgnTimingToExecute_deg <= CAL_PIP_BASE_DEG)
        {
            bSecondarySparkEnable = false;
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Compute fixed dwell*/
        prvFixedDwellComputation();

        /*If Engine speed is less than or equal to CAL_IDLE_STABILITY_MAX_RPM, recompute ignition angle*/
        if(gIgnition.EngineSpeed_rpm <= CAL_IDLE_STABILITY_MAX_RPM)
        {
            IgnitionTimingFromAdv_deg = ((CAL_PIP_WIDTH_DEG + CAL_PIP_BASE_DEG) - IgnTimingToExecute_deg);
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Compute Zero to dwell start if dwell is not starting at advance pulse and spark is not cutoff */
        if((!bStartDwellAtAdvPulse) && (!bSparkCutoff))
        {
            tZeroToDwellStart = ComputeDwellStartTime(bEndDwellAtZeroPulse, tAdvToIgnition, tDiffZeroToZero, tDiffAdvToZero, tDwellTime);

            /*Updated for adding ignition main computational delay compensation in dwell start scheduling*/
            tCapturedIgnCounter = TCNT_IGN;
            if(tCapturedIgnCounter > tCapturedZeroPulse)
            {
                tZeroToDwellStart = tZeroToDwellStart - (tCapturedIgnCounter - tCapturedZeroPulse);
                /*Modified as per review points from Pramod*/
            }
            else
            {
                tZeroToDwellStart = tZeroToDwellStart - (MAX_TIMER_COUNTS - tCapturedZeroPulse) - tCapturedIgnCounter;
                /*Modified as per review points from Pramod*/
            }

            /*Schedule  Zero to dwell start*/
            prvScheduleIgnTimerA(tZeroToDwellStart);
            bDwellStartInitiated = true;
        }
        else
        {
            /*Nothing to do here*/
        }

        /*Retain timer counts for further calculations*/
        tPreExtendedZeroPulse = tExtendedZeroPulse;
        tPrevDiffZeroToZero = tDiffZeroToZero;
        tPrevDiffAdvToZero = tDiffAdvToZero;
        bZeroPulseReceived = false;


    }
    else
    {
        /*Nothing to do here*/
    }
}


/*****************************************************************************************
function          : Ignition_TimerB_CompareISR
description       : This function is called in timer B compare ISR of application code.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_TimerB_CompareISR(void)
{
    /*If delayed dwell start is enabled, start dwell*/
    if(bStartDelayedDwell && !bSparkCutoff)
    {
        prvTurnOnPrimaryIgbt();
        bStartDelayedDwell = false;
    }
    else
    {
        /*Nothing to do here*/
    }
}


/*****************************************************************************************
function          : Ignition_TimerA_CompareISR
description       : This function is called in timer A compare ISR of application code.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_TimerA_CompareISR(void)
{
    /*Turn OFF IGBT if dwell end initiated*/
   if(bDwellEndInitiated)
   {
        prvTurnOffPrimaryIgbt();
        bDwellEndInitiated = false;

        /*Schedule seccondary dwell start if enabled */
        if(bSecondarySparkEnable == true)
        {
            prvScheduleIgnTimerA(CAL_PRI_TO_SEC_SPARK_TIME_USEC);
            bSecDwellStartInitiated = true;
        }
        else
        {
            /*Nothing to do here*/
        }
   }  /*Turn ON IGBT if dwell start initiated*/
   else if((bDwellStartInitiated) && (bSparkCutoff == false))
   {
        prvTurnOnPrimaryIgbt();
        bDwellStartInitiated = false;
   } /*Turn ON IGBT if secondary dwell start initiated*/
    /*bZeroReceived monitored to avoid secondary dwell start after zero pulse received. dt. 17-Mar-18*/
   else if((bSecDwellStartInitiated == true) && (bSparkCutoff == false) && (bZeroReceived == false))
   {
        prvTurnOnPrimaryIgbt();
        bSecDwellStartInitiated = false;
        /*Schedule seccondary dwell end if enabled */
        if(bSecondarySparkEnable == true)
        {
            prvScheduleIgnTimerA(CAL_SEC_DWELL_TIME);
            bSecDwellEndInitiated = true;
        }
        else
        {
            /*Nothing to do here*/
        }
   } /*Turn OFF IGBT if secondary dwell end initiated*/
   else if((bSecDwellEndInitiated == true) && (bSparkCutoff == false))
   {
        prvTurnOffPrimaryIgbt();
        bSecDwellEndInitiated = false;
   }
   else
   {
        /*Nothing to do here*/
   }
}


/*****************************************************************************************
function          : Ignition_AdvPulseIsr
description       : This function executes delyed dwell start and schedules the end of dwell.
Input parameters  : Captured advance pulse time, Timer overflow near adv status flag.
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_AdvPulseIsr( uint16_t tAdvPulse )
{
    //Cranking_AdvPulseIsr();
    if(eCpsLastState == CPS_LAST_STATE_ADV)
    {
	    return;
    }
    else
    {
        /*Nothing to do here*/
    }

    tExtendedAdvPulse = tAdvPulse + tExtendedTimer;

    /*Zero to Advance timer count computation*/
    tDiffZeroToAdv = tExtendedAdvPulse - tExtendedZeroPulse;

    /*Reset counter for Engine OFF detection*/
    nOveflowsSinceLastZero = 0;

    /*If first crsnk pulse received*/
    if(!gIgnition.bEngineOn)
    {
        gIgnition.bEngineOn = true;
        bFirstCrankPulse = true;
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Added to avoid secondary dwell start after zero pulse received. dt. 17-Mar-18*/
    bZeroReceived = false;

    /*If engine speed is less than CAL_IDLE_MAX_RPM then, execute idlestability*/
    prvIdleStability();
    prvCheckPulserCoilInterchanged(bFirstCrankPulse);

    /*If delayed dwell start enabled compute advance to dwell start timer counts*/
    if(bDelayedDwellStartEnable)
    {
        if(tAdvToIgnition >= (tDwellTime + DWELL_COUNT_OFFSET) )
        {
            tAdvToDwellStart = tAdvToIgnition - tDwellTime;
        }
        else
        {
            bDelayedDwellStartEnable = true;
        }
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Check if dwell is started or not*/
    if(!bSparkCutoff)
    {
        if(bDelayedDwellStartEnable == true)
        {
            /*Schedule delayed dwell*/
            prvScheduleIgnTimerB(tAdvToDwellStart);
            bStartDelayedDwell = true;
        }
        else
        {
            /*Turn ON IGBT if dwell is not started yet*/
            prvTurnOnPrimaryIgbt();
            bDwellStartInitiated = false;
        }
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Acquire timer counts*/
	tCapturedAdvPulse = TCNT_IGN;

    /* Logic added to compensate the computational delay of any other high priority interrupt
   Logic is not accounted for timer overflow because common interrupt is used for timer
   overflow, advance and zero pulse routine*/
    /*Modified as per review points from Pramod*/
    tAdvToIgnition = tAdvToIgnition -(tCapturedAdvPulse - tAdvPulse) ;


    if((!bEndDwellAtZeroPulse) && (bEngineCranked))
    {
        prvScheduleIgnTimerA(tAdvToIgnition);
        bDwellEndInitiated = true;
    }
    else
    {
        /*Nothing to do here*/
    }
    bAdvPulseReceived = false;
    eCpsLastState = CPS_LAST_STATE_ADV;
}


/*****************************************************************************************
function          : Ignition_ZeroPulseIsr
description       : This function is called in zero pulse ISR. It checks for extended dwell
                    and schedules it, Otherwise executes dwell end. It captures the time
                    references required for ignition scheduling and load computations.
Input parameters  : Captured zero pulse time, Timer overflow near zero status flag.
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_ZeroPulseIsr(uint16_t tZeroPulse )
{
    if(eCpsLastState == CPS_LAST_STATE_ZERO)
    {
	    return;
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Acquire timer counts*/
    tCapturedZeroPulse = tZeroPulse;

    tExtendedZeroPulse = tZeroPulse + tExtendedTimer;

    /*Compute Zero to Zero timer counts*/
    tDiffZeroToZero = tExtendedZeroPulse - tPreExtendedZeroPulse;

    /*Compute Advance to Zero timer counts*/
    tDiffAdvToZero = tExtendedZeroPulse - tExtendedAdvPulse;

    /*Check if extended dwell enabled*/
    prvExtendedDwellCheck();

    /*Reset counter for Engine OFF detection*/
    nOveflowsSinceLastZero = 0;

    /*If first crank pulse received*/
    if(!gIgnition.bEngineOn)
    {
        gIgnition.bEngineOn = true;
        bFirstCrankPulse = true;
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Check if dwell end at zero pulse*/
    if((bEndDwellAtZeroPulse == true))
    {

        if(bExtendDwell == true)
        {
            /*Schedule extended dwell*/
            prvScheduleIgnTimerA(CAL_EXTENDED_DWELL_COUNTS);
            bDwellEndInitiated = true;
        }
        else
        {
             /*Turn OFF IGBT if dwell end at zero pulse*/
            prvTurnOffPrimaryIgbt();
            bDwellEndInitiated = false;
        }
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Turn OFF IGBT if dwell is not yet ended*/
    if((bDwellEndInitiated == true) && (IgnTimingToExecute_deg >= CAL_PIP_BASE_DEG) && (bExtendDwell == false))
    {
        prvTurnOffPrimaryIgbt();
        bDwellEndInitiated = false;
    }
    else
    {
        /*Nothing to do here*/
    }

    /* By default turn off secondary spark */
    if((bSecDwellEndInitiated == true) && (IgnTimingToExecute_deg >= CAL_PIP_BASE_DEG) && (bExtendDwell == false))
    {
        prvTurnOffPrimaryIgbt();
        bSecDwellEndInitiated = false;
    }
    else
    {
        /*Nothing to do here*/
    }

    bStartDelayedDwell = false;
    bZeroPulseReceived = true;

    /*Added to avoid secondary dwell start after zero pulse received. dt. 17-Mar-18*/
    bZeroReceived = true;
    eCpsLastState = CPS_LAST_STATE_ZERO;
   // Cranking_ZeroPulseIsr1(tDiffAdvToZero);
}


/*****************************************************************************************************************************************
function          : Ignition_TimerC_OverflowISR
description       : This function is called in timer overflow routine periodically at every
                    65.535 ms. It is used for engine off detection and extended timer implementation.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************************************************************/
void Ignition_TimerC_OverflowISR(void)
{
    tExtendedTimer = tExtendedTimer + MAX_TIMER_COUNTS;


   /***********************************************************************************************************************************************
    Engine off detection logic updated @TVSM to add one more logic to prevent spark firing in a particular condition when spark cap is
    outside the engine.
    Issue : ISGSW-29 : After engine off detection, dwell end is causing noise on CPS line which in turn is getting detected as ADV pulse in software
    causing dwell to start again and repeated spark after engine off detection annd followed noise being detected as adv.
    Resolution : After engine OFF detected CPS last state is forced to adv state which in turn will ignore next adv and after next TimerC
    overflow CPS last state is made reset to enable detection of both pulser signals.
   ************************************************************************************************************************************************/
    nOveflowsSinceLastZero++;
    if(nOveflowsSinceLastZero == 10)
    {
	    eCpsLastState = CPS_LAST_STATE_ADV;
	    gIgnition.bEngineOn = false;
    }
    else if(nOveflowsSinceLastZero >= 11)
    {
	    eCpsLastState = CPS_LAST_STATE_NONE;
	    nOveflowsSinceLastZero = 12;
    }
    /***************************************************************************************************************************************************/

    /*Check if corrective Map is enabled or not*/
    if(bCorrectiveMapEnabled)
    {
        nOverflowForCorrectiveAngle++;
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Staurate nOverflowForCorrectiveAngle if exceeds */
    if(nOverflowForCorrectiveAngle >= COUNT_CORRECTIVE_MAP)
    {
        nOverflowForCorrectiveAngle = COUNT_CORRECTIVE_MAP + 1;

        /*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
        /*Declare corrective execution time is elapsed*/
        bCorrectiveExeTimeElapsed = true;
    }
    else
    {
        /*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
        /*Declare corrective execution time is not elapsed*/
        bCorrectiveExeTimeElapsed = false;
    }

    /*Check if engine speed is greater than  CAL_DEACC_STABLE_RPM for COUNT_DECELERATION_MAP time */
    if((gIgnition.EngineSpeed_rpm >= CAL_DEACC_STABLE_RPM)&&(nDeaccEnableAfterCrank < (COUNT_DECELERATION_MAP + 1)))
    {
        nDeaccEnableAfterCrank++;
    }
    else
    {
        /*Nothing to do here*/
    }

    /*Check if engine speed is greater than  CAL_PA_DEACC_STABLE_RPM for COUNT_PA_DECELERATION_MAP time */
    if((gIgnition.EngineSpeed_rpm >= CAL_PA_DEACC_STABLE_RPM)&&( nPwrAsstDeaccEnableAfterCrank < (COUNT_PA_DECELERATION_MAP + 1)))
    {
        nPwrAsstDeaccEnableAfterCrank++;
    }
    else
    {
        /*Nothing to do here*/
    }


    /*If engine is OFF*/
    if(!gIgnition.bEngineOn)
    {
        prvTurnOffPrimaryIgbt();
        ResetLoadVariables();
        prvResetIgnitionVariables();
        prvResetAccelerationVariables();
        prvResetDeaccelerationVariables();
        prvResetCorrectiveMapVariables();
        prvPwrAsstResetAccelerationVariables();
        prvPwrAsstResetDeaccelerationVariables();
    }
    else
    {
        /*Nothing to do here*/
    }
}


/*******************************************************************************************************************************************
function          : prvExtendedDwellCheck
description       : This function used to to check conditions(reverse rotation) for enabling extended dwell.
Input parameters  : None
Output parameters : None
Return value      : None
*********************************************************************************************************************************************/
static void prvExtendedDwellCheck(void)
{
    /*Decide execution of extended dwell*/
    if((CAL_EXTENDED_DWELL_ENABLE) && (gIgnition.EngineSpeed_rpm < CAL_RPM_EXTENDED_DWELL))
    {
        if((tDiffAdvToZero >= CAL_EXTEND_DWELL_STRIP_COUNTS) && (tDiffZeroToZero > tPrevDiffZeroToZero) && (bFirstCrankPulse == false))
        {
            bExtendDwell = true;
        }
        else
        {
            bExtendDwell = false;
        }
   }
   else
   {
        /*Nothing to do here*/
   }
}


/***************************************************************************************************************************************
function          : prvFixedDwellComputation
description       : This function is used to schedule fixed dwell
Input parameters  : None
Output parameters : None
Return value      : None
**************************************************************************************************************************************/
static void prvFixedDwellComputation(void)
{
    if((gIgnition.EngineSpeed_rpm > RPM_FIXED_DWELL) || (!bEngineCranked))
    {
        bDelayedDwellStartEnable = false;
    }
    /*Condition modified to execute computed dwell start below CAL_HW_TRG_MIN for CAL_N_ENGINE_REV_DWN cycles.*/
    /*bCorrectiveMapEnabled is monitored for the same. dt. 17-Mar-18 */
    else if((gIgnition.EngineSpeed_rpm < RPM_FIXED_DWELL) && ((bCorrectiveMapEnabled) \
           || ((gIgnition.EngineSpeed_rpm > CAL_RPM_HW_TRG_MAX) && (!bCorrectiveMapEnabled))) && ((bEndDwellAtZeroPulse) || (tAdvToIgnition > tDwellTime)))
    {
        if(bEndDwellAtZeroPulse)
        {
            if(tPrevDiffAdvToZero > tDwellTime)
            {
                tAdvToDwellStart = tPrevDiffAdvToZero - tDwellTime;
                if(tAdvToDwellStart < DWELL_COUNT_OFFSET)
                {
                    tAdvToDwellStart = DWELL_COUNT_OFFSET;
                }
                else
                {
                    /*Nothing to do here*/
                }
                bDelayedDwellStartEnable = true;
                bStartDwellAtAdvPulse = true;
            }
            else
            {
                bDelayedDwellStartEnable = false;
            }
        }
        else
        {
            tAdvToDwellStart = tAdvToIgnition - tDwellTime;
            if(tAdvToDwellStart > DWELL_COUNT_OFFSET)
            {
                bDelayedDwellStartEnable = true;
                bStartDwellAtAdvPulse = true;
            }
            else
            {
                bDelayedDwellStartEnable = false;
            }
        }
    }
    else
    {
        bDelayedDwellStartEnable = false;
    }
}


/*****************************************************************************************
function          : prvScheduleIgnTimerA
description       : This function is used to schedule timer A
Input parameters  : Variable to be loaded in timer A
Output parameters : None
Return value      : None
*****************************************************************************************/
static void prvScheduleIgnTimerA(uint16_t timerACount)
{
    //IGNITION_STOP_TIMER_A();
    T_IGN_A(timerACount);
    //IGNITION_START_TIMER_A();
}


/*****************************************************************************************
function          : prvScheduleIgnTimerB
description       : This function is used to schedule timer B
Input parameters  : Variable to be loaded in timer B
Output parameters : None
Return value      : None
*****************************************************************************************/
static void prvScheduleIgnTimerB(uint16_t timerBCount)
{
   // IGNITION_STOP_TIMER_B();
    T_IGN_B(timerBCount);
   // IGNITION_START_TIMER_B();
}


/************************************************************************************************************************
function          : EngineSpeedTrendDetect
description       : This function is used to detect whether engine sped is increasing, decreasing or stable
Input parameters  : None
Output parameters : None
Return value      : None
***********************************************************************************************************************/
static void prvEngineSpeedTrendDetect(void)
{
    static uint8_t nRevRpmIncreasing = 0, nRevRpmDecreasing = 0, nRevRpmStable = 0;

    if(gIgnition.EngineSpeed_rpm > PrevEngineSpeed_rpm)
    {
        nRevRpmDecreasing = 0;
        if(eEngSpeedTrend != ENG_SPEED_TREND_INCREASING)
        {
            eEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
            nRevRpmIncreasing++;
            /*If engine speed is increasing for consecutive CAL_NO_CYCLES_RPM_INCREASING cycles */
            if(nRevRpmIncreasing >= CAL_NO_CYCLES_RPM_INCREASING)
            {
                eEngSpeedTrend = ENG_SPEED_TREND_INCREASING;
                nRevRpmIncreasing = CAL_NO_CYCLES_RPM_INCREASING;
            }
            else
            {
                /*Nothing to do here*/
            }
        }
        else
        {
             /*Nothing to do here*/
        }
    }
    else if(gIgnition.EngineSpeed_rpm < PrevEngineSpeed_rpm)
    {
        nRevRpmIncreasing = 0;
        if(eEngSpeedTrend != ENG_SPEED_TREND_DECREASING)
        {
            eEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
            nRevRpmDecreasing++;
            /*If engine speed is decreasing for consecutive CAL_NO_CYCLES_RPM_DECREASING cycles */
            if(nRevRpmDecreasing >= CAL_NO_CYCLES_RPM_DECREASING)
            {
                eEngSpeedTrend = ENG_SPEED_TREND_DECREASING;
                nRevRpmDecreasing = CAL_NO_CYCLES_RPM_DECREASING;
            }
            else
            {
                 /*Nothing to do here*/
            }
        }
        else
        {
             /*Nothing to do here*/
        }
    }
    else
    {
        eEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
        nRevRpmIncreasing = 0;
        nRevRpmDecreasing = 0;
    }

    /*If engine speed trend is unkown*/
    if(eEngSpeedTrend == ENG_SPEED_TREND_UNKNOWN)
    {
        nRevRpmStable++;
         /*If engine speed is not increasing or decreasing for consecutive CAL_NO_CYCLES_RPM_STABLE cycles */
        if(nRevRpmStable >= CAL_NO_CYCLES_RPM_STABLE)
        {
            eEngSpeedTrend = ENG_SPEED_TREND_STABLE;
            nRevRpmStable = CAL_NO_CYCLES_RPM_STABLE;
        }
        else
        {
            /*Nothing to do here*/
        }
    }
    else
    {
        nRevRpmStable = 0;
    }
}

/************************************************************************************************************************
function          : EngineSpeedTrendDetect
description       : This function is used to detect whether engine sped is increasing, decreasing or stable
Input parameters  : None
Output parameters : None
Return value      : None
***********************************************************************************************************************/
static void prvPwrAsstEngineSpeedTrendDetect(void)
{
    static uint8_t nRevRpmIncreasing = 0, nRevRpmDecreasing = 0, nRevRpmStable = 0;

    /*decide engine speed trend*/
    if(gIgnition.EngineSpeed_rpm > PrevEngineSpeed_rpm )
    {
        /*Reset decreasing counter*/
        nRevRpmDecreasing = 0;
        if(ePwrAsstEngSpeedTrend != ENG_SPEED_TREND_INCREASING)
        {
            ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
            nRevRpmIncreasing++;
            /*If engine speed is increasing for consecutive CAL_NO_CYCLES_RPM_INCREASING cycles */
            if(nRevRpmIncreasing >= CAL_PA_NO_CYCLES_RPM_INCREASING)
            {
                ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_INCREASING;
                nRevRpmIncreasing = CAL_PA_NO_CYCLES_RPM_INCREASING;
            }
            else
            {
                /*Nothing to do here*/
            }
        }
        else
        {
            /*Nothing to do here*/
        }
    }
    else if(gIgnition.EngineSpeed_rpm < PrevEngineSpeed_rpm )
    {
        /*Reset increasing counter*/
        nRevRpmIncreasing = 0;
        if(ePwrAsstEngSpeedTrend != ENG_SPEED_TREND_DECREASING)
        {
            ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
            nRevRpmDecreasing++;
            /*If engine speed is decreasing for consecutive CAL_NO_CYCLES_RPM_DECREASING cycles */
            if(nRevRpmDecreasing >= CAL_PA_NO_CYCLES_RPM_DECREASING)
            {
                ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_DECREASING;
                nRevRpmDecreasing = CAL_PA_NO_CYCLES_RPM_DECREASING;
            }
            else
            {
                /*Nothing to do here*/ 
            }
        }
        else
        {
            /*Nothing to do here*/
        }
    }
    else
    {
        ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN; 
        nRevRpmIncreasing = 0;
        nRevRpmDecreasing = 0;       
    }

    /*If engine speed trend is unkown*/
    if(ePwrAsstEngSpeedTrend == ENG_SPEED_TREND_UNKNOWN)
    {
        nRevRpmStable++;
        if(nRevRpmStable >= CAL_PA_NO_CYCLES_RPM_STABLE)
        {
            ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_STABLE;
            nRevRpmStable = CAL_PA_NO_CYCLES_RPM_STABLE ;
        }
        else
        {
            /*Nothing to do here*/
        }
    }
    else
    {
        nRevRpmStable = 0;
    }
}

/***********************************************************************************************************************************************
function          : prvAccelerationDetection
description       : This function is used to detect aceleration if delta RPM is more than set parameter for set revolutions.
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvAccelerationDetection(void)
{
    uint16_t DeltaAcc_rpm = 0;
    static uint8_t nRevAcceleration = 0;

    if( eEngSpeedTrend == ENG_SPEED_TREND_INCREASING  && CAL_ENABLE_ACC_CURVE)
    {
        DeltaAcc_rpm = gIgnition.EngineSpeed_rpm - PrevEngineSpeed_rpm;
        if(!bAccelerationCurveEnabled)
        {
            if(DeltaAcc_rpm > CAL_ACC_DELTA_RPM_LIMIT)
            {
                nRevAcceleration++;
                /*Enable acceleration if Engine speed is increasing with delta RPM CAL_ACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_ACC*/
                if(nRevAcceleration >= CAL_NO_CYCLES_ACC)
                {
                    bAccelerationCurveEnabled = true;
                    nRevAcceleration = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                nRevAcceleration = 0;
            }
        }
        else
        {
            /*Disable acceleration if Engine speed is not increasing with delta RPM CAL_ACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_ACC*/
            if(DeltaAcc_rpm <= CAL_ACC_DELTA_RPM_LIMIT)
            {
                nRevAcceleration++;
                if(nRevAcceleration >= CAL_NO_CYCLES_ACC)
                {
                    bAccelerationCurveEnabled = false;
                    nRevAcceleration = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                nRevAcceleration = 0;
            }
        }
    }
    else
    {
        bAccelerationCurveEnabled = false;
        nRevAcceleration = 0;
    }
}

/***********************************************************************************************************************************************
function          : prvPwrAsstAccelerationDetection
description       : This function is used to detect aceleration if delta RPM is more than set parameter for set revolutions in case of Power Assist.
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvPwrAsstAccelerationDetection(void)
{
    uint16_t DeltaAcc_rpm = 0;
    static uint8_t nRevAcceleration = 0;

    if(ePwrAsstEngSpeedTrend == ENG_SPEED_TREND_INCREASING  && CAL_PA_ENABLE_ACC )
    {
        DeltaAcc_rpm = gIgnition.EngineSpeed_rpm - PrevEngineSpeed_rpm;
        if(!bPwrAsstAccelerationEnabled)
        {
            if(DeltaAcc_rpm > CAL_PA_ACC_DELTA_RPM_LIMIT)
            {
                nRevAcceleration++;
                /*Enable acceleration if Engine speed is increasing with delta RPM CAL_ACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_ACC*/
                if(nRevAcceleration >= CAL_PA_NO_CYCLES_ACC)
                {
                    bPwrAsstAccelerationEnabled = true;
                    nRevAcceleration = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                nRevAcceleration = 0;
            }
        }
        else
        {
            /*Disable acceleration if Engine speed is not increasing with delta RPM CAL_ACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_ACC*/
            if(DeltaAcc_rpm <= CAL_PA_ACC_DELTA_RPM_LIMIT)
            {
                nRevAcceleration++;
                if(nRevAcceleration >= CAL_PA_NO_CYCLES_ACC)
                {
                    bPwrAsstAccelerationEnabled = false;
                    nRevAcceleration = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                nRevAcceleration = 0;
            }
        }
    }
    else
    {
        bPwrAsstAccelerationEnabled = false;
        nRevAcceleration = 0;
    }
}

/*********************************************************************************************************************************************
function          : prvDeaccelerationDetection
description       : This function is used to detect deaceleration if delta RPM is less than set parameter for set revolutions.
Input parameters  : None
Output parameters : None
Return value      : None
**********************************************************************************************************************************************/
static void prvDeaccelerationDetection(void)
{
    uint16_t DeltaDeacc_rpm = 0;
    static uint8_t nRevDeacceleration = 0;

    if(eEngSpeedTrend == ENG_SPEED_TREND_DECREASING && CAL_ENABLE_DEACC_MAP)
    {
        DeltaDeacc_rpm = PrevEngineSpeed_rpm - gIgnition.EngineSpeed_rpm;
        if(!bDeaccelerationCurveEnabled)
        {
            if(DeltaDeacc_rpm > CAL_DEACC_DELTA_RPM_LIMIT)
            {
                nRevDeacceleration++;
                /*Enable deacceleration if Engine speed is decreasing with delta RPM CAL_DEACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_DEACC*/
                /*After first crank pulse, deceleration is not enabled till CAL_DEACC_N_SEC_ENABLE elapsed for engine speed greater than CAL_DEACC_STABLE_RPM */
                if((nRevDeacceleration >= CAL_NO_CYCLES_DEACC)&& (nDeaccEnableAfterCrank > COUNT_DECELERATION_MAP))
                {
                    bDeaccelerationCurveEnabled = true;
                    nDeaccEnableAfterCrank = COUNT_DECELERATION_MAP + 1;
                    nRevDeacceleration = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                nRevDeacceleration = 0;
            }
        }
    }
    else
    {
        nRevDeacceleration = 0;
        /*If engine speed incresed above CAL_DEACC_RPM_MAX during decelration then, disable deceleration */
        if(gIgnition.EngineSpeed_rpm >= CAL_DEACC_RPM_MAX)
        {
            bDeaccelerationCurveEnabled = false;
        }
        else
        {
            /*Nothing to do here*/
        }
    }
}

/*********************************************************************************************************************************************
function          : prvPwrAsstDeaccelerationDetection
description       : This function is used to detect deaceleration if delta RPM is less than set parameter for set revolutions.
Input parameters  : None
Output parameters : None
Return value      : None
**********************************************************************************************************************************************/
static void prvPwrAsstDeaccelerationDetection(void)
{
    uint16_t DeltaDeacc_rpm = 0;
    static uint8_t nRevDeacceleration = 0;

    if(ePwrAsstEngSpeedTrend == ENG_SPEED_TREND_DECREASING && CAL_PA_ENABLE_DEACC )
    {
        DeltaDeacc_rpm = PrevEngineSpeed_rpm - gIgnition.EngineSpeed_rpm;
        if(!bPwrAsstDeaccelerationEnabled)
        {
            if(DeltaDeacc_rpm > CAL_PA_DEACC_DELTA_RPM_LIMIT)
            {
                nRevDeacceleration++;
                /*Enable deacceleration if Engine speed is decreasing with delta RPM CAL_DEACC_DELTA_RPM_LIMIT for consecutive CAL_NO_CYCLES_DEACC*/
                /*After first crank pulse, deceleration is not enabled till CAL_DEACC_N_SEC_ENABLE elapsed for engine speed greater than CAL_DEACC_STABLE_RPM */
                if((nRevDeacceleration >= CAL_PA_NO_CYCLES_DEACC)&& ( nPwrAsstDeaccEnableAfterCrank > COUNT_PA_DECELERATION_MAP))
                {
                    bPwrAsstDeaccelerationEnabled = true;
                    nPwrAsstDeaccEnableAfterCrank = COUNT_PA_DECELERATION_MAP + 1;
                    nRevDeacceleration = 0;
                }
            }
            else
            {
                /*Nothing to do here*/
            }
        }
        else
        {
            nRevDeacceleration = 0;
        }
    }
    else
    {
        nRevDeacceleration = 0;
        /*If engine speed incresed above CAL_DEACC_RPM_MAX during decelration then, disable deceleration */
        if(gIgnition.EngineSpeed_rpm >= CAL_PA_DEACC_RPM_MAX )
        {
            bPwrAsstDeaccelerationEnabled = false;
        }
        else
        {
            /*Nothing to do here*/
        }
    }
}
/*********************************************************************************************************************************************
function          : prvDeaccelerationDetection
description       : This function returns the deacceleration status
Input parameters  : None
Output parameters : boolean
Return value      : None
**********************************************************************************************************************************************/
bool Ignition_DeaccelerationStatus(void)
{
 return bDeaccelerationCurveEnabled;
}
/******************************************************************************************************************************************
function          : prvIdleStability
description       : This function is used to recompute tAdvToIgnition for idle stability purpose  using recent Zero to Adv
                    time below idle max rpm parameter.
Input parameters  : None
Output parameters : None
Return value      : None
**************************************************************************************************************************************************/
static void prvIdleStability(void)
{
    uint32_t tDiffZeroToAdv;

    /*Compute Advance to Igniton timer counts if engine speed is less than or equal to CAL_IDLE_STABILITY_MAX_RPM*/
    if(gIgnition.EngineSpeed_rpm <= CAL_IDLE_STABILITY_MAX_RPM)
    {
        tDiffZeroToAdv = tExtendedAdvPulse - tExtendedZeroPulse;
        tAdvToIgnition = (uint16_t)(((uint32_t)(IgnitionTimingFromAdv_deg) * tDiffZeroToAdv) / (360 - CAL_PIP_WIDTH_DEG)) - SparkHardwareOffset;
    }
    else
    {
        /*Nothing to do here*/
    }
}


/*********************************************************************************************************************************************
function          : prvResetAccelerationVariables
description       : This function clears acceleration related variables
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvResetAccelerationVariables(void)
{
    bAccelerationCurveEnabled = false;
    eEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
}

/*********************************************************************************************************************************************
function          : prvPwrAsstResetAccelerationVariables
description       : This function clears acceleration related variables
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvPwrAsstResetAccelerationVariables(void)
{
    bPwrAsstAccelerationEnabled = false;
    ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
}

/*********************************************************************************************************************************************
function          : prvResetDeaccelerationVariables
description       : This function clears deacceleration related variables
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvResetDeaccelerationVariables(void)
{
    bDeaccelerationCurveEnabled = false;
    eEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN;
    nDeaccEnableAfterCrank = 0;
}

/*********************************************************************************************************************************************
function          : prvResetDeaccelerationVariables
description       : This function clears deacceleration related variables
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvPwrAsstResetDeaccelerationVariables(void)
{
    bPwrAsstDeaccelerationEnabled = false;
    ePwrAsstEngSpeedTrend = ENG_SPEED_TREND_UNKNOWN; 
    nPwrAsstDeaccEnableAfterCrank = 0;   
}


/*********************************************************************************************************************************************
function          : prvResetCorrectiveMapVariables
description       : This function clears deacceleration related variables
Input parameters  : None
Output parameters : None
Return value      : None
*************************************************************************************************************************************************/
static void prvResetCorrectiveMapVariables(void)
{
    bCorrectiveMapEnabled = false;
    nOverflowForCorrectiveAngle = 0;
    nEngineRevUp = 0;
    nEngineRevDwn = 0;
    bCorrectiveToHwTrg = false;
    /*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
    bCorrectiveExeTimeElapsed = false;
    nEngineRev = 0;
    DeltaIgnAngle_deg = 0;
    nVariableAngle = 0;
    bVariableAngle = true;

}


/*****************************************************************************************
function          : prvResetIgnitionVariables
description       : This function clears ignition variables
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void prvResetIgnitionVariables(void)
{
    bEngineCranked = false;
    bStartDwellAtAdvPulse = true;
    bEndDwellAtZeroPulse = true;
    bDwellStartInitiated = false;
    bDwellEndInitiated = false;
    gIgnition.EngineSpeed_rpm = 0;
    bPulserCoilInterchanged = false;
    nPulserFault = 0;
    LoadValueFilt_percent = 0;
    IgnTimingToExecute_deg = 0;
    bDelayedDwellStartEnable = false;
    bZeroReceived = false;

}

/*********************************************************************************************************************************************
function          : prvDecideDigitalFiring
description       : This function is used to check whether to enable analog or digital firing
Input parameters  : None
Output parameters : None
Return value      : None
**********************************************************************************************************************************************/
void prvDecideDigitalFiring(uint16_t Engine_rpm, uint16_t AnalogFiring_rpm)
{
    /*If engine speed is less than AnalogFiring_rpm then, execute Hardware triggered dwell*/
    if((Engine_rpm < AnalogFiring_rpm) || (!bEngineCranked))
    {
        bStartDwellAtAdvPulse = true;
        bEndDwellAtZeroPulse = true;
    }
    else
    {
        bStartDwellAtAdvPulse = false;
        bEndDwellAtZeroPulse = false;
    }
}


/*****************************************************************************************
function          : Ignition_TimerD_CaptureISR
description       : This function is called in timer D compare ISR of application code.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
void Ignition_TimerD_CaptureISR(void)
{
}


/*****************************************************************************************
function          : prvTurnOnPrimaryIgbt
description       : This function is called to turn on primary IGBT.
Input parameters  : None
Output parameters : None
Return value      : None
*****************************************************************************************/
static void prvTurnOnPrimaryIgbt(void)
{
    if((gIgnition.bEnabled == true) &&
       (gIgnition.bExcessCurrentDetected == false))
    {
        TURN_ON_PRI_IGBT()
    }
    else
    {
        /*Nothing to do here*/
    }
}

static void prvTurnOffPrimaryIgbt(void)
{
    if(gIgnition.bEnabled == true)
    {
         TURN_OFF_PRI_IGBT();
    }
    else
    {
        if(bJustDisabled == true)
        {
            bJustDisabled = false;
            TURN_OFF_PRI_IGBT();
        }
        else
        {
            /*Nothing to do here*/
        }

    }
}

/*****************************************************************************************
function          : prvComputePipWidth
description       : This function used to find pip width.
Input parameters  : time reference of adv to zero pulse and zero to zero pulse
Output parameters : pip width
Return value      : uint8_t
*****************************************************************************************/
static uint8_t prvComputePipWidth(uint32_t DiffAdvToZero, uint32_t DiffZeroToZero)
{
    uint8_t PipWidth;

    PipWidth = (uint8_t)((DiffAdvToZero*360)/DiffZeroToZero);

    return PipWidth;
}

/*****************************************************************************************
function          : prvCheckPulserCoilInterchanged
description       : This function used to find pulser coil interchange fault
Input parameters  : first crank pulse flag
Output parameters : None
Return value      : None
*****************************************************************************************/
static void prvCheckPulserCoilInterchanged(bool bFirstPulse)
{

    if((tDiffAdvToZero > tDiffZeroToAdv) && (!bFirstPulse))
    {
        nPulserFault ++;
        if(nPulserFault > COUNT_CPS_FAULT)
        {
            bPulserCoilInterchanged = true;
        }
        else
        {
            /*Nothing to do here*/
        }
    }
    else
    {
        nPulserFault = 0;
    }
}


/*****************************************************************************************
function          : prvDecideCorrectiveMapExecution
description       : This function used to decide corrective map execution based on RPM and time
Input parameters  : Engine speed,
Output parameters : None
Return value      : bool
*****************************************************************************************/
static bool prvDecideCorrectiveMapExecution(uint16_t Engine_rpm)
{
    static bool bCorrective_Map_Enabled = false;

    /*Check if engine speed is less than CAL_CORRECTIVE_MAP_RPM_MAX then enable corrective map */
    if(Engine_rpm < CAL_CORRECTIVE_MAP_RPM_MAX)
    {
        /*Check if engine speed is greater than or equal to CAL_RPM_HW_TRG_MIN for consecutive CAL_N_ENGINE_REV_UP cycles*/
        if(Engine_rpm >= CAL_RPM_HW_TRG_MIN)
        {
            nEngineRevUp++;                             /*Engine revolution up counter*/
            nEngineRevDwn = 0;                           /*Engine revolution down counter*/

            if(nEngineRevUp >= CAL_N_ENGINE_REV_UP )
            {
                nEngineRevUp = CAL_N_ENGINE_REV_UP + 1;
                bCorrective_Map_Enabled = true;         /*Enable corrective map execution*/

		/*bCorrectiveToHwTrg added to maintain cyclic hysteresis during corrective to HW_TRG transition*/
		bCorrectiveToHwTrg = true;
            }
            else
            {
                /*Nothing to do here*/
            }

        } /*Check if engine speed is less than CAL_RPM_HW_TRG_MIN for consecutive CAL_N_ENGINE_REV_DWN cycles*/
        else  /*Removed if() as it is not required*/
        {
            nEngineRevDwn++;                            /*Engine revolution down counter*/
            nEngineRevUp = 0;                           /*Engine revolution up counter*/

	    /*bCorrectiveToHwTrg added to maintain cyclic hysteresis during corrective to HW_TRG transition*/
            bCorrectiveToHwTrg = false;

            if(nEngineRevDwn >= CAL_N_ENGINE_REV_DWN )
            {
                nEngineRevDwn = CAL_N_ENGINE_REV_DWN + 1;
                bCorrective_Map_Enabled = false;      /*disable corrective map execution*/

		/*bCorrectiveToHwTrg added to maintain cyclic hysteresis during corrective to HW_TRG transition*/
		bCorrectiveToHwTrg = true;

                /*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
                /*If corrective execution time is not yet elapsed reset nOverflowForCorrectiveAngle counter*/
                if (bCorrectiveExeTimeElapsed == false)
                {
                    nOverflowForCorrectiveAngle = 0;
                }
                else
                {
                    /*Nothing to do here*/
                }
            }
            else
            {
                  /*Nothing to do here*/
            }

        }
    }
    else /*Disable corrective map if engine speed exceeds CAL_CORRECTIVE_MAP_RPM_MAX */
    {
        bCorrective_Map_Enabled = false;
        nOverflowForCorrectiveAngle = COUNT_CORRECTIVE_MAP + 1;

        /*Modified as per requirement specified by TVSM dt. 22 Dec 17*/
        bCorrectiveExeTimeElapsed = true;
    }


    /*Check if Engine speed is less than CAL_CORRECTIVE_MAP_RPM_MAX */
    if(bCorrective_Map_Enabled == true)
    {
        /*Check if Timer counter for corrective map is not elapsed*/
        if (nOverflowForCorrectiveAngle < COUNT_CORRECTIVE_MAP)
        {
            bCorrective_Map_Enabled = true;
        }
        else
        {
            bCorrective_Map_Enabled = false;
        }
    }
    else
    {
        /*Nothing to do here*/
    }


    return bCorrective_Map_Enabled;  /*Return to bCorrectiveMapEnabled*/
}