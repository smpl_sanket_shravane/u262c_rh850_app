#ifndef IGN_MACRO_H
#define IGN_MACRO_H

#include "userdefine.h"
#include "generic_switch.h"
#include "map2bsp.h"

#define TURN_ON_PRI_IGBT()                  mSetIgbtState(IGBT_1, HIGH);
#define TURN_OFF_PRI_IGBT()                 mSetIgbtState(IGBT_1, LOW);
#define T_IGN_A(u16Time_us)                 mScheduleTimer(TIMER_0, u16Time_us)
#define T_IGN_B(u16Time_us)                 mScheduleTimer(TIMER_1, u16Time_us)
#define TCNT_IGN                            (FTM1->CNT)

#endif /* IGN_MACRO_H */
