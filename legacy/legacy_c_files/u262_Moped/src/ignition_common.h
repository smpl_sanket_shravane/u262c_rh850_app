/**************************************************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: load_computation.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 11.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are common across all ignition variants.
***************************************************************************************************************************************************/

#ifndef IGNITION_COMMON_H
#define IGNITION_COMMON_H

#include "S32K144.h"
#include "simple_types.h"
#include "userdefine.h"

uint16_t ComputeEngineSpeed(uint32_t DiffZerotoZero, uint32_t PrevDiffZerotoZero, bool *bFirstCrankPulse, uint8_t IgnTimerResolution);
void CheckEngineCranked(uint16_t Engine_Rpm, uint16_t EngineCranked_Rpm, bool *EngineCranked, uint8_t nHwTriggerCycles);
void CutOffIgnition(uint16_t rpm, uint16_t SparkCuoffRpm, uint16_t SparkResumeRpm, bool *bCutoffSpark, uint8_t nCutoffCycles);
uint16_t ComputeAdvToIgnTime(int16_t IgnTiming_deg, uint32_t StripRefTime, uint8_t HardwareOffset);
uint16_t ComputeDwellStartTime(bool bDwellEndAtZero, uint16_t TimeAdvToIgn, uint32_t DiffZerotoZero, uint32_t DiffAdvToZero, uint16_t DwellTime );

#endif