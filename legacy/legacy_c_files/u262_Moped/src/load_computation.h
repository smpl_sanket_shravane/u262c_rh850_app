/**************************************************************************************************************************************************
* Copyright (C) 2016, SEDEMAC Mechatronics Pvt. Ltd. All rights reserved.
*
* Module name: load_computation.c
* Version: 0
* Author: Priyanka Nagare
* Date created: 11.11.2016
* External references: None.
*
* Module description:
* This module implements the functions which are related to engine load computation.
***************************************************************************************************************************************************/
#ifndef LOAD_COMPUTATION_H
#define LOAD_COMPUTATION_H

#include "S32K144.h"
#include "simple_types.h"
#include "userdefine.h"

void StrokeDetectionAlgo1(uint32_t DeltaZeroToZero, uint32_t prevDeltaZeroToZero);
void StrokeDetectionAlgo2(uint32_t DeltaZeroToZero, uint32_t prevDeltaZeroToZero, uint8_t offset);
uint32_t ComputeRawLoadAlgo1(uint32_t DeltaZeroToZero, uint32_t prevDeltaZeroToZero);
uint32_t ComputeRawLoadAlgo2(uint32_t DeltaZeroToZero, uint32_t prevDeltaZeroToZero);
uint16_t NormalizeRawLoad(uint32_t FullLoadValue, uint32_t ComputedRawLoad);
void SaturateFiltDuringFastWOT(uint16_t RawLoadValue_percent, uint16_t FilteredLoadValue_percentage, uint8_t FastWotDeltaLoad, uint8_t FastWotMaxLoad, uint8_t nCyclesFastWot);
uint16_t LoadDigitalFilter(uint16_t LoadRawVal, uint16_t LoadFilterCoeff);
void ResetLoadVariables(void);

#endif