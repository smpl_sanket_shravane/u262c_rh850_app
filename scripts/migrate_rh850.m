%%
Old_BSP_Mdl_List = struct2table(libinfo(bdroot, 'RegExp', 'on', 'ReferenceBlock', '^isg_bsp/'));
Old_BSP_Mdl_List = sortrows(Old_BSP_Mdl_List, 'Block', 'ascend');

%%
commandwindow;
for i=1:size(Old_BSP_Mdl_List, 1)
    SelectedBlock = cell2mat(Old_BSP_Mdl_List.Block(i));
    disp(['Processing ', SelectedBlock]);
    SelectedBlockRef = get_param(SelectedBlock, 'ReferenceBlock');
    set_param(SelectedBlock, 'LinkStatus', 'inactive');
    
    NewBlockRef = regexprep(SelectedBlockRef, '^isg_bsp/', 'isg_bsp_rh850/');
    try
        set_param(SelectedBlock, 'AncestorBlock', NewBlockRef);
        set_param(SelectedBlock, 'LinkStatus', 'restore');
    catch ME
        warning(ME.message);
        open_system(SelectedBlock, 'tab');
        set_param(SelectedBlock, 'AncestorBlock', SelectedBlockRef, 'LinkStatus', 'restore');
    end
end

New_BSP_Mdl_List = struct2table(libinfo(bdroot, 'RegExp', 'on', 'ReferenceBlock', '^isg_bsp'));