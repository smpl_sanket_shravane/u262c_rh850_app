from crank import CrankRecorder

import pycan.comm as comm
import time
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn

def cli():
    cr = CrankRecorder()

    try:
        comm.init(comm.BAUD_RATE_1M)

        while True:
            can_msg = comm.recv()
            cr.update(can_msg)

            df = collect_cranking_plot()
            if df is not None:
                df[['Vbat', 'BemfSpeed', 'BHspinSpeed', 'Uamp', 'Ia', 'Ibus', 'Theta', 'L', 'Utheta', 'PhaseError']].plot(x=df['Time'])
                df.to_csv('{}.csv'.format(crank_idx))
                plt.show()
                crank_idx += 1
    except KeyboardInterrupt:
        comm.terminate()
        sys.exit()
    except comm.CodeError:
        comm.terminate()
        sys.exit()


if __name__ == "__main__":
    cli()