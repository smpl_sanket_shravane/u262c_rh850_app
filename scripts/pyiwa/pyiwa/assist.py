import pycan.comm as comm
import time
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn


def _to_int8(x):
    if x > 2**7:
        x = -(2**8 - x)
    return x

def _to_int16(x):
    if x > 2**15:
        x = -(2**16 - x)
    return x

def collect_cranking_plot(timeout=1, CRANK_ID=0xD1, IDLING_ID=0xD2):
    start_time = time.time()
    can_msgs_crank = []
    can_msgs_idling = []
    row_list = []
    while (time.time() - start_time < timeout):
        can_msg = None
        try:
            can_msg = comm.recv()
        except comm.CodeError as err:
            can_msg = None
        if can_msg is not None:
            if can_msg.identifier == CRANK_ID:
                can_msgs_crank.append(can_msg)
                start_time = time.time()
            if can_msg.identifier == IDLING_ID:
                can_msgs_idling.append(can_msg)
                start_time = time.time()
    if not can_msgs_crank:
        return None

    for i, can_msg in enumerate(can_msgs_crank):
        Vbat = can_msg.data_bytes[3]/10 # Units is Volts
        Theta = _to_int8(can_msg.data_bytes[4])*2 # Units is deg
        Ia = _to_int8(can_msg.data_bytes[5]) # Units is ampere
        Uamp = can_msg.data_bytes[6] # Units is 0 to 100
        Ibus = _to_int8(can_msg.data_bytes[7]) # Units is ampere
        L = can_msg.data_bytes[0] # Unit is uH
        BemfSpeed = _to_int8(can_msg.data_bytes[2])*20 # Unit is RPM
        BHspinSpeed = can_msg.data_bytes[1]*20 # Unit is RPM

        row_list.append({"Vbat": Vbat, 
                         "BemfSpeed": BemfSpeed,
                         "BHspinSpeed": BHspinSpeed,
                         "Uamp": Uamp,
                         "Ia": Ia,
                         "Ibus": Ibus,
                         "Theta": Theta,
                         "L": L,
                         "Utheta": 90,
                         "PhaseError": 0,
                         "Time": 166e-3 * i})

    for i, can_msg in enumerate(can_msgs_idling):
        Vbat = can_msg.data_bytes[3]/10 # Units is Volts
        Ia = _to_int8(can_msg.data_bytes[5]) # Units is ampere
        Uamp = can_msg.data_bytes[6] # Units is 0 to 100
        Ibus = _to_int8(can_msg.data_bytes[7]) # Units is ampere
        Utheta = can_msg.data_bytes[4] # Units is deg
        PhaseError = _to_int8(can_msg.data_bytes[0])
        BemfSpeed = _to_int16(256*can_msg.data_bytes[1] + (can_msg.data_bytes[2])) # Unit is RPM
        row_list.append({"Vbat": Vbat, 
                         "BemfSpeed": BemfSpeed,
                         "BHspinSpeed": 0,
                         "Uamp": Uamp,
                         "Ia": Ia,
                         "Ibus": Ibus,
                         "Theta": 0,
                         "Utheta": Utheta,
                         "PhaseError": PhaseError,
                         "L": 0,
                         "Time": (166e-3 * (i + len(can_msgs_crank)))})
    return pd.DataFrame(row_list)


if __name__ == "__main__":
    crank_idx = 1
    try:
        comm.init(comm.BAUD_RATE_1M)
        while True:
            df = collect_cranking_plot()
            print(df, flush=True)
            if df is not None:
                df[['Vbat', 'BemfSpeed', 'BHspinSpeed', 'Uamp', 'Ia', 'Ibus', 'Theta', 'L', 'Utheta', 'PhaseError']].plot(x=df['Time'])
                df.to_csv('{}.csv'.format(crank_idx))
                plt.show()
                crank_idx += 1
    except KeyboardInterrupt:
        comm.terminate()
        sys.exit()
    except comm.CodeError:
        comm.terminate()
        sys.exit()
