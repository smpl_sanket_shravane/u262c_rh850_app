import copy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pycan.sym import parse_sym_file
from pycan.trace import parse_trace_file

def _to_int8(x):
    if x > 2**7:
        x = -(2**8 - x)
    return x

def _to_int16(x):
    if x > 2**15:
        x = -(2**16 - x)
    return x

latest_set = {
    'Vbat': 0,
    'BemfSpeed': 0,
    'OpMode': 0,
    'Ibat': 0,
    'Iamp': 0,
    'MotorTemperature': 0,
    'TorquePct': 0,
    'WheelSpeed': 0,
    }

def cli():
    sym_menu = parse_sym_file('../pcan/ISG_with_Assist.sym')
    can_msgs = parse_trace_file('trace_files/creep_ON_OFF.trc', trace_format='peak')
    #named_can_msgs = list(map(sym_menu.raw_to_named_can_msg, can_msgs))
    dataset = []
    for can_msg in can_msgs:
        if can_msg.identifier == 0xC1:
            latest_set['OpMode'] = can_msg.data_bytes[3]
        if can_msg.identifier == 0xC2:
            latest_set['MotorTemperature'] = _to_int8(can_msg.data_bytes[3])
            latest_set['Iamp'] = _to_int8(can_msg.data_bytes[4])*2
            latest_set['TorquePct'] = can_msg.data_bytes[2]
            latest_set['Ibus'] = _to_int16(can_msg.data_bytes[6]*256 + can_msg.data_bytes[7])/10
        elif can_msg.identifier == 0xC3:
            latest_set['BemfSpeed'] = _to_int16(can_msg.data_bytes[6]*256 + can_msg.data_bytes[7])
            latest_set['WheelSpeed'] = _to_int16(can_msg.data_bytes[2]*256 + can_msg.data_bytes[3])/10
        elif can_msg.identifier == 0xC4:
            latest_set['Vbat'] = (can_msg.data_bytes[1]*256 + can_msg.data_bytes[2])/1000
            dataset.append(copy.copy(latest_set))

    df = pd.DataFrame(dataset)
    df[['Ibus', 'WheelSpeed']].plot()
    df.to_csv('trace_files/creep_ON_OFF.csv')
    plt.show()


if __name__ == '__main__':
    cli()



