import pycan.comm as comm
import time
import pandas as pd


def _to_int8(x):
    if x > 2**7:
        x = -(2**8 - x)
    return x

def _to_int16(x):
    if x > 2**15:
        x = -(2**16 - x)
    return x

STATES = ['WAITING', 'COLLECTING', 'DONE']

class CrankRecorder:

    def __init__(self):
        self.state = 'WAITING'

        self.df = None
        self.can_msgs_crank = []
        self.can_msgs_idling = []
        self.start_time = time.time()
        self.row_list = []

    def update(can_msg, timeout=1, CRANK_ID=0xD1, IDLING_ID=0xD2):

        if self.state == 'WAITING':
            if can_msg.identifier == CRANK_ID:
                self.state = 'COLLECTING'

        elif self.state == 'COLLECTING':
            if can_msg.identifier == CRANK_ID:
                self.can_msgs_crank.append(can_msg)
                self.start_time = time.time()
            if can_msg.identifier == IDLING_ID:
                self.can_msgs_idling.append(can_msg)
                self.start_time = time.time()

        if (time.time() - self.start_time < timeout):
            can_msg = None
        if not self.can_msgs_crank:
            return None

        for i, can_msg in enumerate(self.can_msgs_crank):
            Vbat = can_msg.data_bytes[3]/10 # Units is Volts
            Theta = _to_int8(can_msg.data_bytes[4])*2 # Units is deg
            Ia = _to_int8(can_msg.data_bytes[5]) # Units is ampere
            Uamp = can_msg.data_bytes[6] # Units is 0 to 100
            Ibus = _to_int8(can_msg.data_bytes[7]) # Units is ampere
            L = can_msg.data_bytes[0] # Unit is uH
            BemfSpeed = _to_int8(can_msg.data_bytes[2])*20 # Unit is RPM
            BHspinSpeed = can_msg.data_bytes[1]*20 # Unit is RPM

            self.row_list.append({"Vbat": Vbat, 
                             "BemfSpeed": BemfSpeed,
                             "BHspinSpeed": BHspinSpeed,
                             "Uamp": Uamp,
                             "Ia": Ia,
                             "Ibus": Ibus,
                             "Theta": Theta,
                             "L": L,
                             "Utheta": 90,
                             "PhaseError": 0,
                             "Time": 166e-3 * i})

        for i, can_msg in enumerate(self.can_msgs_idling):
            Vbat = can_msg.data_bytes[3]/10 # Units is Volts
            Ia = _to_int8(can_msg.data_bytes[5]) # Units is ampere
            Uamp = can_msg.data_bytes[6] # Units is 0 to 100
            Ibus = _to_int8(can_msg.data_bytes[7]) # Units is ampere
            Utheta = can_msg.data_bytes[4] # Units is deg
            PhaseError = _to_int8(can_msg.data_bytes[0])
            BemfSpeed = _to_int16(256*can_msg.data_bytes[1] + (can_msg.data_bytes[2])) # Unit is RPM
            self.row_list.append({"Vbat": Vbat, 
                             "BemfSpeed": BemfSpeed,
                             "BHspinSpeed": 0,
                             "Uamp": Uamp,
                             "Ia": Ia,
                             "Ibus": Ibus,
                             "Theta": 0,
                             "Utheta": Utheta,
                             "PhaseError": PhaseError,
                             "L": 0,
                             "Time": (166e-3 * (i + len(self.can_msgs_crank)))})

            self.df = pd.DataFrame(self.row_list)
            to_return = copy.deepcopy(self.df)
            self.df = None
            return to_return
