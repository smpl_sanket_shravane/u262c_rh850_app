from setuptools import setup
import pyiwa


setup(name='pyiwa',
    version=pyiwa.__version__,
    description='Package to interface with ISG with Assist controller',
    long_description='',
    url='http://www.sedemac.com',
    author='Anay Joshi',
    author_email='anay.joshi@sedemac.com',
    license='SEDEMAC',
    packages=['pyiwa',
    ],
    install_requires=[
    ],
    scripts=[
    ],
    entry_points={
        'console_scripts': {
            'iwa-logger= pyiwa.logger:cli',	
        }
    },
    include_package_data=True,
    zip_safe=False)
