% Module       : get_motoring_tables.m
% Author(s)    : Samyak Kamble, Anay Joshi
% Date created : 11th July 2019
% Last updated : 11th July 2019
% Description  : This module outputs calibration tables to be used in power
%                assist mode. It takes as input a config dictionary which
%                contains values of machine characteristics & constraints
%                such as voltage & current limits. The function then
%                returns two tables (U_THETA_z & U_AMP_z) which are 2D
%                arrays with x-axis as speed & y-axis as torque percent.
%                The tables indicate the values of of U_theta & U_amp to be
%                applied to get required fraction of max_torque given a
%                certain machine speed.

function [U_THETA_z, U_AMP_z] = get_motoring_tables(config)
                                                            
U_THETA_z = zeros(length(config.SPEED_RANGE), length(config.TORQUE_PCT_RANGE));     
U_AMP_z = zeros(length(config.SPEED_RANGE), length(config.TORQUE_PCT_RANGE)); 

speed_idx = 1;
for Speed_RPM = config.SPEED_RANGE
    torque_pt_idx = 1;
    
    Tmax = get_Tmax(Speed_RPM, config);
    for Torque_Pct = config.TORQUE_PCT_RANGE      
        
        fprintf('Computing for Vbat = %.1fV, Speed = %d RPM, Torque = %d %%\n', ...
                config.Vbat, Speed_RPM, Torque_Pct);
            
        Treq = Tmax*Torque_Pct/100;  
        [Utheta, Uamp, Torque] = get_U_for_Treq(Speed_RPM, Treq, config);        
        U_THETA_z(speed_idx, torque_pt_idx) = Utheta;
        U_AMP_z(speed_idx, torque_pt_idx) = Uamp;
        
        torque_pt_idx = torque_pt_idx + 1;
    end
    speed_idx = speed_idx + 1;
end

end