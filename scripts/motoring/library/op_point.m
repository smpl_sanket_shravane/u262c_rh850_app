% Module       : op_point.m
% Author(s)    : Anay Joshi, Samyak Kamble
% Date created : 11th July 2019
% Last updated : 11th July 2019
% Description  : This is a base level function used to find the amount of
%                torque and line current generated by application of
%                certain U_amp and U_theta by the controller, at certin
%                speed (& given a certain machine configuration + other
%                voltage & current constraints mentioned in config)
function [I_RMS, Torque] = op_point(Uamp, Utheta, Speed_RPM, config)

Vbat = config.Vbat;
L = config.L;
R = config.R;
PP = config.PP;
R_mag = config.R_mag;
I_sat = config.I_sat;
BemfConst = config.BemfConst;
PM_Flux = BemfConst*60/(1000*2*pi*sqrt(3)*PP);

% Applied voltage vector, per phase
% This assumes that rotor field is aligned with coil A
U_RMS = (cos(Utheta*pi/180) + 1i*sin(Utheta*pi/180))*Uamp*Vbat/(100*sqrt(6));

% Bemf voltage vector, per phase
% This assumes that rotor field is aligned with coil A
Vb_RMS = 1i*(Speed_RPM * BemfConst/1000)/sqrt(6);

% Complex Impedance (Ohm)
Z = R + (Speed_RPM*PP*pi/30)*L*1i;                             

% Current due to effective voltage vector
I_RMS = (U_RMS - Vb_RMS)/Z;

% Resolving along DQ axes
Id = sqrt(2)*real(I_RMS);
Iq = sqrt(2)*imag(I_RMS);

Ld = L*(1 - tanh((R_mag + Id)/I_sat)^2);
Lq = L*(1 - tanh(Iq/I_sat)^2);

Torque = (1.5*PP)*(PM_Flux*Iq + (Ld - Lq)*Id*Iq);
    
end